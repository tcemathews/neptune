using Sce.PlayStation.Core.Graphics;
using Neptune.Core;
using Neptune.Display.Graphics;
using Neptune.Utility;

namespace NeptuneTest
{
	public class AppMain
	{
		public static GraphicsContext gc;
		
		public static void Main (string[] args)
		{
			gc = new GraphicsContext();
			//RunSokobanTest();
			//RunTestA();
			RunTestB();
		}
		
		public static void RunTestA()
		{
			Processor p = new Processor(gc);
			p.Process = new Process();
			p.Process.Screen.Color.SetRGB(0, 255, 255);
			p.Init();
			
			p.Process.Screen.Cache.AddTexture("/Application/textures/sokoban.png", TextureUtility.GetTexturePackerMap("/Application/textures/sokoban.xml"));
			p.Process.Screen.Cache.CompressTextures();
			p.Process.Screen.Cache.AddFont("kannada", FontUtility.LoadFontAtlasFromFile("/Application/fonts/kannada.fnt", "kannada"));
			
			p.Process.Operators.Add(new Player());
			p.Process.Operators.Add(new Ball());
			
			Label label = new Label("kannada");
			label.ClipFrame.Width = 955;
			label.ClipFrame.Height = 300;
			label.Text = "The quick brown fox jumped over the hedge!1234567890-=_+)(*&^%$#@!`.,<>/?'\";:~";
			p.Process.Screen.AddGraphic(label);

			while ( p.Alive ) {
				//label.Text = p.FrameRate.ToString();
				p.Step();
			}
			p.Dispose();
		}
		
		public static void RunTestB()
		{
			Processor p = new Processor(gc);
			p.Process = new Process();
			p.Process.Screen.Color.SetRGB(0, 0, 0);
			p.Init();
			
			// Cache resources here
			p.Process.Screen.Cache.AddTexture("/Application/textures/sokoban.png", TextureUtility.GetTexturePackerMap("/Application/textures/sokoban.xml"));
			p.Process.Screen.Cache.CompressTextures();
			p.Process.Screen.Cache.AddFont("kannada", FontUtility.LoadFontAtlasFromFile("/Application/fonts/kannada.fnt", "kannada"));
			p.Process.Screen.Cache.AddMesh("sphere", MeshUtility.GetSphere(.75f));
			p.Process.Screen.Cache.AddMesh("palmtree", MeshUtility.GetMeshObj("/Application/models/palmtree.obj", true));
			
			// Do 3D stuff here...
			
			Label label = new Label("kannada");
			p.Process.Screen.AddGraphic(label);
			
			Model palmModel = new Model(null, "palmtree");
			palmModel.Z = -15;
			palmModel.Y = -5;
			p.Process.Screen.AddGraphic(palmModel);
			
			while ( p.Alive ) {
				float dt = 1.0f / 60.0f;
				
				palmModel.Transform3D.Rotation.Y += dt * 20.0f;
				
				label.Text = p.FrameRate.ToString();
				p.Step();
			}
			p.Dispose();
		}
		
		public static void RunSokobanTest()
		{
			Processor p = new Sokoban.SokobanGame(gc);
			p.Init();
			while ( p.Alive ) {
				p.Step();
			}
			p.Dispose();
		}
	}
}
