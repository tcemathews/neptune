using System;
using Sce.PlayStation.Core;
using Neptune.Core;
using Neptune.Display.Rendering;
using Neptune.Display.Graphics;
using Neptune.Utility;

namespace Sokoban
{
	public class Character : Operator
	{
		public Picture Sprite;
		public float X;
		public float Y;
		public float MoveDistance;
		public float MoveSpeed;
		public float DestX;
		public float DestY;
		public Rectangle Hitbox;
		
		public Character ()
		{
			DestX = X = 0.0f;
			DestY = Y = 0.0f;
			MoveSpeed = 256.0f;
			MoveDistance = 64.0f;
			Hitbox = new Rectangle(0, 0, 64, 64);
		}
		
		override public void ManageScreen(Screen screen)
		{
			if ( !Expired ) screen.AddGraphic(Sprite);
			else if ( Expired ) screen.RemoveGraphic(Sprite);
		}
		
		public Vector2 GetGlobalCenter()
		{
			return new Vector2(X + Hitbox.Width * 0.5f, Y + Hitbox.Height * 0.5f);	
		}
		
		public Rectangle GetGlobalHitbox()
		{
			return new Rectangle(X, Y, Hitbox.Width, Hitbox.Height);
		}
		
		public bool IsMoving()
		{
			return X != DestX || Y != DestY;	
		}
		
		public void SetPosition(float x, float y)
		{
			X = DestX = x;
			Y = DestY = y;
		}
		
		public void MoveTo(float x, float y)
		{
			DestX = x;
			DestY = y;
		}
		
		public void UpdateMovement(double dt)
		{
			if ( IsMoving() ) {
				float dx = X - DestX;
				float dy = Y - DestY;
				float dist = (float)Math.Sqrt(dx * dx + dy * dy);
				float speed = (float)(MoveSpeed * dt);
				
				if ( dist <= speed ) {
					X = DestX;
					Y = DestY;
				}
				else {
					float angle = (float)Math.Atan2(DestY - Y, DestX - X);
					X += (float)(Math.Cos(angle) * speed);
					Y += (float)(Math.Sin(angle) * speed);
				}
			}
		}
		
		public void UpdateGraphics()
		{
			Sprite.X = (int)X;
			Sprite.Y = (int)Y;
		}
	}
}

