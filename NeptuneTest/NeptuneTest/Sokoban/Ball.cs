using System;
using Neptune.Core;
using Neptune.Display.Rendering;
using Neptune.Display.Graphics;

namespace Sokoban
{
	public class Ball : Character
	{
		public Ball () : base()
		{
			Tag = "ball";
			ManageScreenNextStep = true;
			Sprite = new Picture("objects/water-ball");
		}
		
		public override void Update (double dt)
		{
			UpdateMovement(dt);
			UpdateGraphics();
		}
	}
}

