using System;
using Sce.PlayStation.Core;
using Sce.PlayStation.Core.Graphics;
using Neptune.Core;
using Neptune.Utility;

namespace Sokoban
{
	public class SokobanGame : Processor
	{
		public SokobanGame (GraphicsContext context) : base(context)
		{
			
		}
		
		public override void Init ()
		{
			base.Init ();
			Process = new Process();
			Process.Screen.Color.SetRGB(255, 255, 255);
			
			// Preload graphics
			Process.Screen.Cache.AddTexture("/Application/textures/sokoban.png", TextureUtility.GetTexturePackerMap("/Application/textures/sokoban.xml"));
			Process.Screen.Cache.CompressTextures();
			
			// Setup the map!
			SetupMap(LoadMap());
		}
		
		public MapData LoadMap()
		{
			MapData d = new MapData();
			d.PlayerPosition = 17;
			
			for ( int i = 0; i < 15; i++ ) {
				d.Tiles[i] = 2;
				d.Tiles[d.Tiles.Length - 1 - i] = 2;
			}
			for ( int i = 1; i < 7; i++ ) {
				d.Tiles[i * 15] = 2;
				d.Tiles[(i * 14) + i - 1] = 2;
			}
			
			d.BallPositions = new System.Collections.Generic.List<int>(new int[]{40, 50});
			d.BucketPositions = new System.Collections.Generic.List<int>(new int[]{35, 63});
			
			return d;
		}
		
		public void SetupMap(MapData data)
		{
			Process.Screen.Graphics.Clear();
			Process.Operators.Clear();
			
			Map map = new Map();
			for ( int i = 0; i < data.Tiles.Length; i++ ) {
				map.Tilemap.SetTile(data.Tiles[i], i);
			}
			Process.Operators.Add(map);
			
			Player player = new Player(map);
			Vector2 p = map.GetPointFromMapIndex(data.PlayerPosition);
			player.SetPosition(p.X, p.Y);
			map.Characters.Add(player);
			Process.Operators.Add(player);
			
			for ( int i = 0; i < data.BallPositions.Count; i++ ) {
				Ball ball = new Ball();
				Vector2 pos = map.GetPointFromMapIndex(data.BallPositions[i]);
				ball.SetPosition(pos.X, pos.Y);
				map.Characters.Add(ball);
				Process.Operators.Add(ball);
			}
			
			for ( int i = 0; i < data.BucketPositions.Count; i++ ) {
				Bucket bucket = new Bucket(map);
				Vector2 pos = map.GetPointFromMapIndex(data.BucketPositions[i]);
				bucket.SetPosition(pos.X, pos.Y);
				map.Characters.Add(bucket);
				Process.Operators.Add(bucket);
			}
		}
	}
}

