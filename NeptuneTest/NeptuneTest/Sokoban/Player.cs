using System;
using Neptune.Controls;
using Neptune.Core;
using Neptune.Display.Rendering;
using Neptune.Display.Graphics;
using Neptune.Utility;

namespace Sokoban
{
	public class Player : Character
	{
		public Controller Pad;
		public Map Map;
		
		public Player ( Map map ) : base()
		{
			Tag = "player";
			Map = map;
			Pad = new Controller(0);
			ManageScreenNextStep = true;
			Sprite = new Picture("objects/player");
		}
		
		public override void Update (double dt)
		{
			Pad.Update();
			UpdateControls(dt);
			UpdateMovement(dt);
			UpdateGraphics();
		}
		
		public void UpdateControls(double dt)
		{
			if ( !IsMoving() ) {
				double direction = -1.0;
				
				if ( Pad.Held(Buttons.Up) ) {
					direction = MapDirection.UP;
				}
				else if ( Pad.Held(Buttons.Down) ) {
					direction = MapDirection.DOWN;
				}
				if ( Pad.Held(Buttons.Right) ) {
					direction = MapDirection.RIGHT;
				}
				else if ( Pad.Held(Buttons.Left) ) {
					direction = MapDirection.LEFT;
				}
				
				if ( direction != -1.0f ) {
					direction = MathUtility.DegreesToRadians(direction); 
					float dx = (float)(X + Math.Cos(direction) * MoveDistance);
					float dy = (float)(Y + Math.Sin(direction) * MoveDistance);
					
					if ( !Map.PointCollidesWithMap(dx, dy) ) {
						Character neighbor = Map.GetCharacterBesideCharacter(this, direction);
						
						if ( neighbor != null && neighbor.Tag == "ball" ) {
							float bdx = (float)(neighbor.X + Math.Cos(direction) * MoveDistance);
							float bdy = (float)(neighbor.Y + Math.Sin(direction) * MoveDistance);
							
							if ( !Map.PointCollidesWithMap(bdx, bdy) ) {
								Character ballNeighbor = Map.GetCharacterBesideCharacter(neighbor, direction);
								if ( ballNeighbor == null || ballNeighbor.Tag != "ball" ) {
									neighbor.MoveTo(bdx, bdy);
									MoveTo(dx, dy);
								}
							}
						}
						else {
							MoveTo(dx, dy);	
						}
					}
				}
			}
		}
	}
}

