using System;
using System.Collections.Generic;
using Sce.PlayStation.Core;
using Neptune.Core;
using Neptune.Display.Graphics;
using Neptune.Utility;

namespace Sokoban
{
	public class MapDirection {
		public const double UP = 270.0;
		public const double RIGHT = 0.0;
		public const double DOWN = 90.0;
		public const double LEFT = 180.0;
	}
	
	public class Map : Operator
	{
		public int TileSize;
		public Tilemap Tilemap;
		public List<Character> Characters;
		
		public Map ()
		{
			Tag = "map";
			TileSize = 64;
			Tilemap = new Tilemap("tilesets/default", TileSize, TileSize, 15, 8);
			Characters = new List<Character>();
			ManageScreenNextStep = true;
		}
		
		public override void ManageScreen(Neptune.Display.Rendering.Screen screen)
		{
			if ( !Expired ) {
				screen.AddGraphic(Tilemap);
			}
			else {
				screen.RemoveGraphic(Tilemap);
			}
		}
		
		public delegate bool CharacterComparison(Character character);
		
		public Character GetCharacterAtXY(float x, float y, CharacterComparison comparison)
		{
			for ( int i = 0; i < Characters.Count; i++ ) {
				Character c = Characters[i];
				Rectangle hitbox = c.GetGlobalHitbox();
				if ( ShapeUtility.RectangleContainsPoint(hitbox.X, hitbox.Y, hitbox.Width, hitbox.Height, x, y) && comparison(c) ) {
					return c;	
				}
			}
			return null;
		}
		
		public Character GetCharacterBesideCharacter(Character character, double direction)
		{
			Rectangle box = character.GetGlobalHitbox();
			int dx = (int)((box.X + box.Width * 0.5) + Math.Cos(direction) * TileSize);
			int dy = (int)((box.Y + box.Height * 0.5) + Math.Sin(direction) * TileSize);
			for ( int i = 0; i < Characters.Count; i++ ) {
				Character vs = Characters[i];
				if ( vs != character ) {
					Rectangle r = vs.GetGlobalHitbox();
					if ( ShapeUtility.RectangleContainsPoint((int)r.X, (int)r.Y, (int)r.Width, (int)r.Height, dx, dy) ) {
						return vs;
					}
				}
			}
			return null;
		}
		
		public bool PointCollidesWithMap(float x, float y)
		{
			int tile = Tilemap.GetTileFromXy((int)(Tilemap.X + x), (int)(Tilemap.Y + y));
			return tile == 2;
		}
		
		public Vector2 GetPointFromMapIndex(int mapindex)
		{
			Vector2	pos = Tilemap.GetXyFromMapIndex(mapindex);
			pos.X += Tilemap.X;
			pos.Y += Tilemap.Y;
			return pos;
		}
	}
}

