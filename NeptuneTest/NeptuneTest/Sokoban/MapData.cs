using System;
using System.Collections.Generic;

namespace Sokoban
{
	public class MapData
	{
		public int PlayerPosition;
		public List<int> BallPositions;
		public List<int> BucketPositions;
		public int[] Tiles;
		
		public MapData ()
		{
			PlayerPosition = 0;
			BallPositions = new List<int>();
			BucketPositions = new List<int>();
			Tiles = new int[15*7];
		}
	}
}

