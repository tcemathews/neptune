using System;
using Neptune.Core;
using Neptune.Display.Graphics;

namespace Sokoban
{
	public class Bucket : Character
	{
		public Map Map;
		
		public Bucket(Map map)
		{
			Map = map;
			Tag = "bucket";
			ManageScreenNextStep = true;
			Sprite = new Picture("objects/seed");
		}
		
		public override void Update (double dt)
		{
			UpdateStatus(dt);
			UpdateGraphics();
		}
		
		public void UpdateStatus(double dt)
		{
			var center = GetGlobalCenter();
			Ball ball = (Ball)Map.GetCharacterAtXY(center.X, center.Y, CompareCharacterForGettingBall);
			if ( ball != null ) {
				Sprite.Visible = false;
			}
			else {
				Sprite.Visible = true;	
			}
		}
		
		public bool CompareCharacterForGettingBall(Character character)
		{
			return character.Tag == "ball";	
		}
	}
}

