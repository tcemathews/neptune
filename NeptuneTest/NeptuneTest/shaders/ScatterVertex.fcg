
// Defaults
/*
float MaterialThickness = .6;
float3 ExtinctionCoefficient = float3(.80, .12, .20);
float4 LightColor = float4(1.0, 1.0, 1.0, 1.0);
float4 BaseColor = float4(1.0, 0.0, 0.0, 0.0);
float4 SpecColor = float4(.9, .9, .9, 1.0);
float SpecPower = 1.0;
float RimScalar = 1.0;
*/

// Tests
float MaterialThickness = .6;
float3 ExtinctionCoefficient = float3(.80, .12, .20);
float4 LightColor = float4(1.0, 1.0, 1.0, 1.0);

float4 SpecColor = float4(.9, .9, .9, 1.0);
float SpecPower = 1;
float RimScalar = 1.0;

float halfLambert(float3 vect1, float3 vect2)
{
    float product = dot(vect1,vect2);
    return product * 0.5 + 0.5;
}

float blinnPhongSpecular(float3 normalVec, float3 lightVec, in float specPower)
{
    float3 halfAngle = normalize(normalVec + lightVec);
    return pow(clamp(dot(normalVec,halfAngle), 0.0, 1.0), specPower);
}

float4 subScatterFS(float3 lightPos, float3 eyeVec, float3 lightVec, float3 vertPos, float3 worldNormal, float4 BaseColor)
{
    float attenuation = 10.0 * (1.0 / distance(lightPos, vertPos));
    float3 eVec = normalize(eyeVec);
    float3 lVec = normalize(lightVec);
    float3 wNorm = normalize(worldNormal);
    
    float4 dotLN = float4(halfLambert(lVec,wNorm) * attenuation);
    //dotLN *= texture2D(Texture, gl_TexCoord[0].xy);
    dotLN *= BaseColor;
    
    float3 indirectLightComponent = float3(MaterialThickness * max(0.0,dot(-wNorm,lVec)));
    indirectLightComponent += MaterialThickness * halfLambert(-eVec,lVec);
    indirectLightComponent *= attenuation;
    indirectLightComponent.r *= ExtinctionCoefficient.r;
    indirectLightComponent.g *= ExtinctionCoefficient.g;
    indirectLightComponent.b *= ExtinctionCoefficient.b;
    
    float3 rim = float3(1.0 - max(0.0,dot(wNorm,eVec)));
    rim *= rim;
    rim *= max(0.0,dot(wNorm,lVec)) * SpecColor.rgb;
    
    float4 finalCol = dotLN + float4(indirectLightComponent,1.0);
    finalCol.rgb += (rim * RimScalar * attenuation * finalCol.a);
    finalCol.rgb += float3(blinnPhongSpecular(wNorm,lVec,SpecPower) * attenuation * SpecColor * finalCol.a * 0.05);
    finalCol.rgb *= LightColor.rgb;
    
    return finalCol;
}

void main(
	float3 in worldNormal : TEXCOORD1,
	float3 in eyeVec : TEXCOORD2,
	float3 in lightVec : TEXCOORD3,
	float3 in vertPos : TEXCOORD4,
	float3 in lightPos : TEXCOORD5,
	float3 in vColor : TEXCOORD6,
	
	float4 out oFragColor : COLOR,
	
	uniform sampler2D uDiffuseMap : TEXUNIT0)
{
	oFragColor = subScatterFS(lightPos, eyeVec, lightVec, vertPos, worldNormal, float4(vColor, .5));
}
