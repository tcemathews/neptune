using System;
using Neptune.Core;
using Neptune.Controls;
using Neptune.Display.Rendering;
using Neptune.Display.Graphics;
using Neptune.Utility;

namespace NeptuneTest
{
	public class Ball : Operator
	{
		public Picture Sprite;
		public float X;
		public float Y;
		public float Speed;
		public float Angle;
		
		public Ball ()
		{
			ManageScreenNextStep = true;
			Sprite = new Picture("objects/player");
			Sprite.Transform2D.Origin.X = Sprite.Transform2D.Origin.Y = 0.5f;
			Sprite.Transform2D.Scale.X = Sprite.Transform2D.Scale.Y = 2.0f;
			X = 100.0f;
			Y = 100.0f;
			Speed = 512.0f;
			Angle = (float)RandomUtility.MinMaxRandomDouble(90.0, 270.0);
		}
		
		public override void ManageScreen(Screen screen)
		{
			if ( !Expired ) screen.AddGraphic(Sprite);
			else if ( Expired ) screen.RemoveGraphic(Sprite);
		}
		
		public override void Update(double dt)
		{
			UpdateControls(dt);
			UpdateGraphics();
		}
		
		public void UpdateControls(double dt)
		{
			float ox = X;
			float oy = Y;
			
			X += (float)(Math.Cos(Angle) * (Speed * dt));
			Y += (float)(Math.Sin(Angle) * (Speed * dt));
			
			if ( X > 960 || X < 0 || Y < 0 || Y > 544 ) {
				X = ox;
				Y = oy;
				Angle += 90.0f;
				if ( Angle > 360.0f ) {
					Angle = Angle - 360.0f;
				}
			}
		}
		
		public void UpdateGraphics()
		{
			Sprite.X = (int)X;
			Sprite.Y = (int)Y;
			Sprite.Transform2D.Rotation += 3.1415927f / 60.0f;
		}
	}
}

