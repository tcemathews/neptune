using System;
using Neptune.Core;
using Neptune.Controls;
using Neptune.Display.Rendering;
using Neptune.Display.Graphics;
using Neptune.Utility;

namespace NeptuneTest
{
	public class Player : Operator
	{
		public Controller Pad;
		public Picture Sprite;
		public float X;
		public float Y;
		public float Speed;
		public Ball BallToAdd;
		
		public Player ()
		{
			Pad = new Controller(0);
			ManageScreenNextStep = true;
			Sprite = new Picture("objects/player");
			Sprite.Transform2D.Origin.X = Sprite.Transform2D.Origin.Y = 0.5f;
			Sprite.Transform2D.Scale.X = Sprite.Transform2D.Scale.Y = 2.0f;
			X = 0.0f;
			Y = 0.0f;
			Speed = 512.0f;
			BallToAdd = null;
		}
		
		public override void ManageScreen(Screen screen)
		{
			if ( !Expired ) screen.AddGraphic(Sprite);
			else if ( Expired ) screen.RemoveGraphic(Sprite);
		}
		
		public override void ManageOperators (System.Collections.Generic.List<Operator> operators)
		{
			if ( !Expired ) {
				if ( BallToAdd != null ) {
					operators.Add(BallToAdd);
					BallToAdd = null;
				}
			}
		}
		
		public override void Update(double dt)
		{
			Pad.Update();
			UpdateControls(dt);
			UpdateGraphics();
		}
		
		public void UpdateControls(double dt)
		{
			if ( Pad.Held(Buttons.Up) ) {
				Y -= Speed * (float)dt;
			}
			else if ( Pad.Held(Buttons.Down) ) {
				Y += Speed * (float)dt;
			}
			
			if ( Pad.Held(Buttons.Right) ) {
				X += Speed * (float)dt;
			}
			else if ( Pad.Held(Buttons.Left) ) {
				X -= Speed * (float)dt;
			}
			
			if ( Pad.Released(Buttons.Btn1) ) {
				BallToAdd = new Ball();	
				BallToAdd.X = (int)RandomUtility.MinMaxRandomInt(100, 900);
				BallToAdd.Y = (int)RandomUtility.MinMaxRandomInt(100, 500);
			}
		}
		
		public void UpdateGraphics()
		{
			Sprite.X = (int)X;
			Sprite.Y = (int)Y;
			Sprite.Transform2D.Rotation += 3.1415927f / 60.0f;
		}
	}
}

