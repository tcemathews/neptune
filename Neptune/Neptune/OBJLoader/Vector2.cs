﻿namespace OBJLoader
{
    public struct Vector2
    {
        public Vector2(float x, float y)
        {
            X = x;
            Y = y;
        }

        public float X;
        public float Y;

        public bool Equals(Vector2 v)
        {
            return X == v.X && Y == v.Y;
        }

        public static Vector2 Zero = new Vector2(0.0f, 0.0f);
    }
}