﻿using System;
using System.Collections.Generic;

namespace OBJLoader
{
    internal class Util
    {
        private static void AddIndexIfExists(ref Vertex vertex, List<Vertex> vertices, List<ushort> indices)
        {
            for (int i = 0; i < vertices.Count; ++i)
            {
                if (vertices[i].Equals(ref vertex))
                {
                    indices.Add((ushort)i);
                    return;
                }
            }

            vertices.Add(vertex);
            indices.Add((ushort)(vertices.Count - 1));
        }

        internal static void AddTriangle(Mesh mesh, MeshObject meshObj, ref Vertex v0, ref Vertex v1, ref Vertex v2)
        {
            AddIndexIfExists(ref v0, mesh.Vertices, meshObj.Indices);
            AddIndexIfExists(ref v1, mesh.Vertices, meshObj.Indices);
            AddIndexIfExists(ref v2, mesh.Vertices, meshObj.Indices);
        }
    }
}
