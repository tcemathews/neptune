﻿namespace OBJLoader
{
    public struct Vertex
    {
        public Vector3 Position;
        public Vector3 Normal;
        public Vector2 TexCoord;

        public Vertex(Vector3 position, Vector3 normal, Vector2 texCoord)
        {
            Position = position;
            Normal = normal;
            TexCoord = texCoord;
        }

		public bool Equals(ref Vertex v)
        {
            return Position.Equals(v.Position) && Normal.Equals(v.Normal) && TexCoord.Equals(v.TexCoord);
        }
    }
}
