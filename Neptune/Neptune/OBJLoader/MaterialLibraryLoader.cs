﻿using System.IO;

namespace OBJLoader
{
    public class MaterialLibraryLoader
    {
        public static void Load(Mesh mesh, string filename)
        {
			try
			{
				FileStream stream = File.OpenRead(filename);
	            StreamReader reader = new StreamReader(stream);
	
	            Material currentMaterial = null;
	
	            string line;
	            while ((line = reader.ReadLine()) != null)
	            {
	                // Clean whitespace
	                line.TrimStart(' ', '\t');
	
	                if (line.StartsWith("newmtl"))
	                {
	                    if (currentMaterial != null)
	                    {
	                        mesh.Materials.Add(currentMaterial);
	                    }
	
	                    string[] tokens = line.Split(' ');
 
	                    currentMaterial = new Material();
	                    currentMaterial.Name = tokens[1];
	
	                    continue;
	                }
	
	                // Don't set parameters on an empty material..
	                if (currentMaterial == null)
	                    continue;
					
					if (line.StartsWith("Ka"))
	                {
	                    string[] tokens = line.Split(' ');
	
	                    currentMaterial.Ka = new Vector3(
	                        float.Parse(tokens[1]),
	                        float.Parse(tokens[2]),
	                        float.Parse(tokens[3]));
	                }
	                else if (line.StartsWith("Kd"))
	                {
	                    string[] tokens = line.Split(' ');
	
	                    currentMaterial.Kd = new Vector3(
	                        float.Parse(tokens[1]),
	                        float.Parse(tokens[2]),
	                        float.Parse(tokens[3]));
	                }
	                else if (line.StartsWith("Ks"))
	                {
	                    string[] tokens = line.Split(' ');
	
	                    currentMaterial.Ks = new Vector3(
	                        float.Parse(tokens[1]),
	                        float.Parse(tokens[2]),
	                        float.Parse(tokens[3]));
	                }
	                else if (line.StartsWith("Ns"))
	                {
	                    string[] tokens = line.Split(' ');
	
	                    currentMaterial.Ns = float.Parse(tokens[1]);
	                }
	                else if (line.StartsWith("d") || line.StartsWith("Tr"))
	                {
	                    string[] tokens = line.Split(' ');
	
	                    currentMaterial.Transparency = float.Parse(tokens[1]);
	                }
	                else if (line.StartsWith("illum"))
	                {
	                    string[] tokens = line.Split(' ');
	
	                    currentMaterial.Illum = (Material.IlluminationModel)int.Parse(tokens[1]);
	                }
	                else if (line.StartsWith("map_Ka"))
	                {
	                    string[] tokens = line.Split(' ');
	
	                    currentMaterial.AmbientMap = tokens[1];
	                }
	                else if (line.StartsWith("map_Kd"))
	                {
	                    string[] tokens = line.Split(' ');
	
	                    currentMaterial.DiffuseMap = tokens[1];
	                }
	                else if (line.StartsWith("map_Ks"))
	                {
	                    string[] tokens = line.Split(' ');
	
	                    currentMaterial.SpecularMap = tokens[1];
	                }
	                else if (line.StartsWith("map_d"))
	                {
	                    string[] tokens = line.Split(' ');
	
	                    currentMaterial.AlphaMap = tokens[1];
	                }
	                else if (line.StartsWith("bump") || line.StartsWith("map_bump"))
	                {
	                    string[] tokens = line.Split(' ');
	
	                    currentMaterial.BumpMap = tokens[1];
	                }
	                else if (line.StartsWith("disp"))
	                {
	                    string[] tokens = line.Split(' ');
	
	                    currentMaterial.DisplacementMap = tokens[1];
	                }
	                else if (line.StartsWith("decal"))
	                {
	                    string[] tokens = line.Split(' ');
	
	                    currentMaterial.DecalMap = tokens[1];
	                }
	            }
	
	            stream.Close();
	
	            if (currentMaterial != null)
	            {
	                mesh.Materials.Add(currentMaterial);
	            }
	        }
			catch (FileNotFoundException e)
			{
			}
	    }
	}
}