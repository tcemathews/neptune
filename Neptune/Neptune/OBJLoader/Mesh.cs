﻿using System;
using System.Collections.Generic;
using System.IO;

namespace OBJLoader
{
    public class Mesh
    {
        public List<Vertex> Vertices = new List<Vertex>();
        public List<MeshObject> Objects = new List<MeshObject>();
        public List<Material> Materials = new List<Material>();
        public bool Textured = false;
		
		public Material GetMaterial(string materialName)
		{
			for (int i = 0; i < Materials.Count; ++i)
			{
				if (Materials[i].Name.Equals(materialName))
					return Materials[i];
			}
			
			return null;
		}
		
        public static Mesh Load(string filename)
        {
            FileStream stream = File.OpenRead(filename);
            StreamReader reader = new StreamReader(stream);

            List<Vector3> vertices = new List<Vector3>();
            List<Vector3> normals = new List<Vector3>();
            List<Vector2> texCoords = new List<Vector2>();

            Mesh mesh = new Mesh();

            MeshObject genericObject = new MeshObject();
            MeshObject currentObject = genericObject;

            string currentMaterialName = null;

            string line;
            while ((line = reader.ReadLine()) != null)
            {
                if (line[0] == 'v')
                {
                    if (line[1] == 't')
                    {
                        string[] tokens = line.Split(' ');
                        texCoords.Add(new Vector2(
                            float.Parse(tokens[1]),
                            float.Parse(tokens[2])));
                    }
                    else if (line[1] == 'n')
                    {
                        string[] tokens = line.Split(' ');
                        normals.Add(new Vector3(
                            float.Parse(tokens[1]),
                            float.Parse(tokens[2]),
                            float.Parse(tokens[3])));
                    }
                    else
                    {
                        string[] tokens = line.Split(' ');
                        vertices.Add(new Vector3(
                            float.Parse(tokens[1]),
                            float.Parse(tokens[2]),
                            float.Parse(tokens[3])));
                    }
                }
                else if (line[0] == 'f')
                {
                    string[] tokens = line.Split(' ');

                    string[] v0Tokens = tokens[1].Split('/');
                    string[] v1Tokens = tokens[2].Split('/');
                    string[] v2Tokens = tokens[3].Split('/');

                    if (String.IsNullOrEmpty(v0Tokens[1]))
                    {
                        Vertex v0 = new Vertex(
                            vertices[(int)Int32.Parse(v0Tokens[0]) - 1],
                            normals[(int)Int32.Parse(v0Tokens[2]) - 1],
                            Vector2.Zero);

                        Vertex v1 = new Vertex(
                            vertices[(int)Int32.Parse(v1Tokens[0]) - 1],
                            normals[(int)Int32.Parse(v1Tokens[2]) - 1],
                            Vector2.Zero);

                        Vertex v2 = new Vertex(
                            vertices[(int)Int32.Parse(v2Tokens[0]) - 1],
                            normals[(int)Int32.Parse(v2Tokens[2]) - 1],
                            Vector2.Zero);

                        Util.AddTriangle(mesh, currentObject, ref v0, ref v1, ref v2);
                    }
                    else
                    {
                        Vertex v0 = new Vertex(
                            vertices[(int)Int32.Parse(v0Tokens[0]) - 1],
                            normals[(int)Int32.Parse(v0Tokens[2]) - 1],
                            texCoords[(int)Int32.Parse(v0Tokens[1]) - 1]);

                        Vertex v1 = new Vertex(
                            vertices[(int)Int32.Parse(v1Tokens[0]) - 1],
                            normals[(int)Int32.Parse(v1Tokens[2]) - 1],
                            texCoords[(int)Int32.Parse(v1Tokens[1]) - 1]);

                        Vertex v2 = new Vertex(
                            vertices[(int)Int32.Parse(v2Tokens[0]) - 1],
                            normals[(int)Int32.Parse(v2Tokens[2]) - 1],
                            texCoords[(int)Int32.Parse(v2Tokens[1]) - 1]);

                        Util.AddTriangle(mesh, currentObject, ref v0, ref v1, ref v2);
                    }
                }
                else if (line[0] == 'o')
                {
                    if (currentObject != genericObject &&
                        currentObject.Indices.Count > 0)
                    {
                        mesh.Objects.Add(currentObject);
                    }

                    string[] tokens = line.Split(' ');

                    if (tokens.Length > 1 && tokens[0] != "null" && tokens[0] != "(null)")
                    {
                        currentObject = new MeshObject();
                        currentObject.Name = tokens[1];
                        currentObject.MaterialName = currentMaterialName;
                    }
                    else
                    {
                        currentObject = genericObject;
                    }
                }
                else if (line.StartsWith("mtllib"))
                {
                    string[] tokens = line.Split(' ');

                    if (tokens.Length > 0)
                    {
                        MaterialLibraryLoader.Load(mesh, "/Application/models/" + tokens[1]);
                    }
                }
                else if (line.StartsWith("usemtl"))
                {
                    string[] tokens = line.Split(' ');

                    if (tokens.Length > 0)
                    {
                        currentObject.MaterialName = tokens[1];
                    }
                    else
                    {
                        currentObject.MaterialName = null;
                    }

                    currentMaterialName = currentObject.MaterialName;
                }
            }

            stream.Close();

            if (genericObject.Indices.Count > 0)
            {
                mesh.Objects.Add(genericObject);
            }

            if (currentObject != genericObject &&
                currentObject.Indices.Count > 0)
            {
                mesh.Objects.Add(currentObject);
            }

            mesh.Textured = texCoords.Count > 0;

            return mesh;
        }
    }
}