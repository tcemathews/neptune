﻿using System;
using System.Collections.Generic;

namespace OBJLoader
{
    public class MeshObject
    {
        public string Name = "Generic";
        public string MaterialName = null;
        public List<ushort> Indices = new List<ushort>();

        public bool HasMaterial
        {
            get { return MaterialName != null; }
        }
    }
}
