﻿namespace OBJLoader
{
    public struct Vector3
    {
        public Vector3(float x, float y, float z)
        {
            X = x;
            Y = y;
            Z = z;
        }

        public float X;
        public float Y;
        public float Z;

        public bool Equals(Vector3 v)
        {
            return X == v.X && Y == v.Y && Z == v.Z;
        }
    }
}