﻿namespace OBJLoader
{
    public class Material
    {
        public enum IlluminationModel
        {
            ColorOnAmbientOff,
            ColorOnAmbientOn,
            Highlight,
            RayTracedReflection,
            RayTracedGlassReflection,
            RayTracedFresnelReflection,
            RayTracedRefractionAndReflection,
            RayTracedRefractionAndFresnelReflection,
            Reflection,
            GlassReflection,
            CastShadowsOntoInvisibleSurfaces
        }

        public string Name = "(null)";
        
        // Ambient Color
        public Vector3 Ka = new Vector3(1.0f, 1.0f, 1.0f);

        // Diffuse color
        public Vector3 Kd = new Vector3(1.0f, 1.0f, 1.0f);

        // Specular color
        public Vector3 Ks = new Vector3(1.0f, 1.0f, 1.0f);

        // Specular weight coefficient (0 - 1000)
        public float Ns = 10.0f;
        
        public float Transparency = 1.0f;

        public IlluminationModel Illum = IlluminationModel.ColorOnAmbientOn;

        public string AmbientMap = null;
        
        public string DiffuseMap = null;

        public string SpecularMap = null;

        public string AlphaMap = null;

        public string BumpMap = null;

        public string DisplacementMap = null;

        public string DecalMap = null;
    }
}
