using System;
using System.Diagnostics;
using Sce.PlayStation.Core.Graphics;
using Neptune.Display.Rendering;

namespace Neptune.Core
{
	public class Processor
	{
		public bool Alive;
		public GraphicsContext Context;
		public Render Render;
		public Process Process;
		public Stopwatch Watch;
		public double Time; // In seconds.
		public double LastTime;
		public float FrameRate;
		
		public Processor(GraphicsContext context) 
		{
			Alive = true;
			LastTime = 0.0;
			Context = context;
			Render = new Render(Context);
		}
		
		virtual public void Init()
		{
			Alive = true;
			LastTime = 0.0;
			Watch = new Stopwatch();
			Watch.Start();
		}
		
		virtual public void Dispose()
		{
			Alive = false;
			Watch.Stop();
			Watch = null;
		}
		
		virtual public void Step()
		{	
			Time = (double)Watch.ElapsedTicks / (double)Stopwatch.Frequency;
			if ( Process != null ) {
				double delta = Time - LastTime;
				Process.Screen.Width = Context.Screen.Width;
				Process.Screen.Height = Context.Screen.Height;
				Process.Update(delta);
				Render.Update(Process.Screen);
				FrameRate = (float)(1.0 / delta);
			}
			LastTime = Time;
		}
	}
}

