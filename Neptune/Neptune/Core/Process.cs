using System;
using System.Collections.Generic;
using Neptune.Display.Rendering;

namespace Neptune.Core
{
	public class Process
	{
		public List<Operator> Operators;
		public Screen Screen;
		
		public Process ()
		{
			Operators = new List<Operator>();
			Screen = new Screen();
		}
		
		public void Update(double dt)
		{
			int count = Operators.Count;
			
			// Manage graphics.
			for ( int i = 0; i < count; i++ ) {
				Operator op = Operators[i];
				if ( op.ManageScreenNextStep ) {
					op.ManageScreen(Screen);
					op.ManageScreenNextStep = false;
				}
				op.ManageOperators(Operators);
			}
			
			// Update the operators.
			for ( int i = 0; i < count; i++ ) {
				Operator op = Operators[i];
				op.Update(dt);
			}
			
			// Remove expired operators.
			for ( int i = 0; i < count; i++ ) {
				Operator op = Operators[i];
				if ( op.Expired ) {
					op.ManageScreen(Screen);
					op.ManageOperators(Operators);
					Operators.Remove(op);
					count--;
				}
			}
		}
	}
}

