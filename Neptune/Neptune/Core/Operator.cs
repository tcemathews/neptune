using System;
using System.Collections.Generic;
using Neptune.Display.Rendering;

namespace Neptune.Core
{
	public class Operator
	{
		public string Tag;
		public bool Expired;
		public bool ManageScreenNextStep;
		
		public Operator ()
		{
			Tag = "Operator";
			Expired = false;
			ManageScreenNextStep = false;
		}
		
		virtual public void ManageScreen(Screen screen)
		{
			// Mutate graphics in the process.
		}
		
		virtual public void ManageOperators(List<Operator> operators)
		{
			// Mutate operators in the process.
		}
		
		virtual public void Update(double dt)
		{
			// Do custom routine here.
		}
	}
}

