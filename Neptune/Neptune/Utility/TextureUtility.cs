using System;
using System.Collections.Generic;
using System.Xml;
using Sce.PlayStation.Core;

namespace Neptune.Utility
{
	public class TextureUtility
	{
		static public Dictionary<string, Rectangle> GetTexturePackerMap(string filename)
		{
			Dictionary<string, Rectangle> map = new Dictionary<string, Rectangle>();
			XmlDocument doc = new XmlDocument();
			doc.Load(filename);
			
			if (doc != null)
			{
				XmlElement atlas = (XmlElement)doc.GetElementsByTagName("TextureAtlas")[0];
				XmlNodeList sprites = atlas.GetElementsByTagName("SubTexture");
				XmlElement firstSprite = (XmlElement)sprites[0];
				
				if (firstSprite.HasAttribute("r"))
				{
					throw new Exception("Error: Texture Packer XML " + filename + " has rotation enabled");
				}
				
				if (firstSprite.HasAttribute("oX")) 
				{
					throw new Exception("Error: Texture Packer XML " + filename + " has trimming enabled");
				}
				
				for (int i = 0; i < sprites.Count; ++i)
				{
					XmlElement sprite = (XmlElement)sprites[i];
					
					int x = System.Convert.ToInt32(sprite.GetAttribute("x"));
					int y = System.Convert.ToInt32(sprite.GetAttribute("y"));
					int w = System.Convert.ToInt32(sprite.GetAttribute("width"));
					int h = System.Convert.ToInt32(sprite.GetAttribute("height"));
					
					map[ sprite.GetAttribute("name") ] = new Rectangle(x, y, w, h);
					//System.Console.WriteLine(sprite.GetAttribute("name") + " " + x + " " + y + " " + w + " " + h);
				}
			}
			else
			{
				throw new Exception("File not found %s\n" + filename);
			}
			
			return map;
		}
	}
}