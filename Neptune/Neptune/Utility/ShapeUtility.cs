using System;

namespace Neptune.Utility
{
	public class ShapeUtility
	{
		static public bool RectangleContainsPoint(int rX, int rY, int rW, int rH, int pX, int pY)
		{
			return pX >= rX && pY >= rY && pX <= rX + rW && pY <= rY + rH;
		}
		
		static public bool RectangleContainsPoint(float rX, float rY, float rW, float rH, float pX, float pY)
		{
			return pX >= rX && pY >= rY && pX <= rX + rW && pY <= rY + rH;
		}
		
		static public bool RectangleInterceptsRectangle()
		{
			// TODO ...
			return false;
		}
	}
}

