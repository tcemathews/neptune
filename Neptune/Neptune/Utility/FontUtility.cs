using System;
using System.Collections.Generic;
using Sce.PlayStation.Core;
using Sce.PlayStation.Core.Graphics;
using Neptune.Display.Atlas;

namespace Neptune.Utility
{	
	public class FontUtility 
	{
		public static FontAtlas LoadFontAtlasFromFile(string filename, string texturekey)
		{
			int size = 0;
			int lineHeight = 0;
			Dictionary<int, FontCharacter> characterMap = new Dictionary<int, FontCharacter>();
			
			/* Split FontFile into lines. Split lines into parameters */
			System.IO.StreamReader file = new System.IO.StreamReader(filename);
			string[] lines = file.ReadToEnd().Split('\n');
			file.Close();
			
			foreach (string line in lines) {
				ParseFontLine(line, ref characterMap, ref size, ref lineHeight);
			}
			
			return new FontAtlas(texturekey, size, lineHeight, characterMap);
		}
		
		private static void ParseFontLine(string line, ref Dictionary<int, FontCharacter> characterMap, ref int size, ref int lineHeight)
		{
			char [] delimiters = {' ','\r','\n'};
			string [] values = line.Split(delimiters);
			int numWords = 0;
			string[] words = new string[values.Length];
			
			foreach (string word in values) {
				if (word.Length > 0) {
					words[ numWords++ ] = word;
				}
			}
			
			/* Warning! This EXPECTS that the file is well formed - add error checking later */
			
			if (numWords > 0)
			{
				if (words[0].Equals("char"))
				{
					if (numWords != 12)
					{
						for ( int i = 0; i < numWords; ++i)
						{
							Console.WriteLine("Argument is '" + words[i] + "'");
						}
						throw new ArgumentOutOfRangeException("numwords not 11");
					}
					int charID = Convert.ToInt32(words[1].Substring(3));
					int x = Convert.ToInt32(words[2].Substring(2));
					int y = Convert.ToInt32(words[3].Substring(2));
					int width = Convert.ToInt32(words[4].Substring(6));
					int height = Convert.ToInt32(words[5].Substring(7));
					int xoffset = Convert.ToInt32(words[6].Substring(8));
					int yoffset = Convert.ToInt32(words[7].Substring(8));
					int xadvance = Convert.ToInt32(words[8].Substring(9));
					
					characterMap.Add(charID, new FontCharacter(x, y, width, height, xoffset, yoffset, xadvance));
					// System.Console.WriteLine(charID + "= width: " + width + " height: " + height + " xoff: " + xoffset + " yoff: " + yoffset);;
				}
				else if (words[0].Equals("info"))
				{
					size = Convert.ToInt32(words[2].Substring(5));
					if (size <= 0)
						throw new ArgumentOutOfRangeException("bad header");
				}
				else if (words[0].Equals("common"))
				{
					lineHeight = Convert.ToInt32(words[1].Substring(11));
					if (size <= 0)
						throw new ArgumentOutOfRangeException("bad header");
				}
			}
		}
	}
}