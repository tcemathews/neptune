using System;

namespace Neptune.Utility
{
	public class MathUtility
	{
		public static double DegreesToRadians(double degrees)
		{
			return (degrees * Math.PI) / 180.0;
		}
		
		public static double RadiansToDegrees(double radians)
		{
			return (radians * 180.0) / Math.PI;	
		}
	}
}

