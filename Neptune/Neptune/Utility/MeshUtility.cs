using System;
using System.Collections.Generic;

using Sce.PlayStation.Core;
using Sce.PlayStation.Core.Graphics;

using Neptune.Display.Rendering;

namespace Neptune.Utility
{
	public class MeshUtility
	{
		static ushort [] cubeIndices = new ushort[]
		{
			0, 1, 2,
			0, 2, 3,
			
			4 + 0, 4 + 1, 4 + 2,
			4 + 0, 4 + 2, 4 + 3,
			
			8 + 0, 8 + 1, 8 + 2,
			8 + 0, 8 + 2, 8 + 3,
			
			12 + 0, 12 + 1, 12 + 2,
			12 + 0, 12 + 2, 12 + 3,
			
			16 + 0, 16 + 1, 16 + 2,
			16 + 0, 16 + 2, 16 + 3,
			
			20 + 0, 20 + 1, 20 + 2,
			20 + 0, 20 + 2, 20 + 3,
		};
		
		static ushort [] pyramidIndices = new ushort[]
		{
			0, 1, 2,
			0, 2, 3,
			
			4, 5, 6,
			7, 8, 9,
			9, 10, 11,
			12, 13, 14
		};
		
		static public Mesh GetMeshObj(string filename, bool merge, float scale = 1.0f)
		{
			OBJLoader.Mesh mesh = OBJLoader.Mesh.Load(filename);
			
			OBJLoader.MeshObject submesh = mesh.Objects[0];
			
			VertexBuffer vbo;
			
			int floatsPerVertex = 3 + 3;
			
			float[] vertices = new float[floatsPerVertex * mesh.Vertices.Count];
			for (int i = 0; i < mesh.Vertices.Count; ++i)
			{
				OBJLoader.Vertex vertex = mesh.Vertices[i];
				
				int index = i * floatsPerVertex;

				vertices[index++] = vertex.Position.X * scale;
				vertices[index++] = vertex.Position.Y * scale;
				vertices[index++] = vertex.Position.Z * scale;
				
				vertices[index++] = vertex.Normal.X;
				vertices[index++] = vertex.Normal.Y;
				vertices[index++] = vertex.Normal.Z;
			}
			
			if (merge)
			{
				int indexCount = 0;
				for (int i = 0; i < mesh.Objects.Count; ++i)
				{
					indexCount += mesh.Objects[i].Indices.Count;
				}
				
				ushort [] indices = new ushort[indexCount];
				for (int i = 0, index = 0; i < mesh.Objects.Count; ++i)
				{
					for (int j = 0; j < mesh.Objects[i].Indices.Count; ++j)
					{
						indices[index + j] = mesh.Objects[i].Indices[j];
					}
					
					index += mesh.Objects[i].Indices.Count;
				}
				
				vbo = new VertexBuffer(mesh.Vertices.Count, indexCount, VertexFormat.Float3, VertexFormat.Float3);
				vbo.SetIndices(indices);
			}
			else
			{
				vbo = new VertexBuffer(mesh.Vertices.Count, submesh.Indices.Count, VertexFormat.Float3, VertexFormat.Float3);
				vbo.SetIndices(submesh.Indices.ToArray());
			}
			
			
			vbo.SetVertices(vertices);
			
			// Terrible naming - rename later
			List <Mesh.MeshDesc> descriptions = new List<Mesh.MeshDesc>();
			Vector4 defaultColor = new Vector4(0, 1.0f, 0, .5f);
			
			int offset = 0;
			for (int i = 0; i < mesh.Objects.Count; ++i)
			{
				Vector4 color = defaultColor;
				
				if (mesh.Objects[i].HasMaterial)
				{
					OBJLoader.Material material = mesh.GetMaterial(mesh.Objects[i].MaterialName);
					color = new Vector4(material.Kd.X, material.Kd.Y, material.Kd.Z, .5f);
				}
				
				descriptions.Add(new Mesh.MeshDesc(offset, mesh.Objects[i].Indices.Count, color));
				offset += mesh.Objects[i].Indices.Count;
			}
			Mesh model = new Mesh(vbo, descriptions);
			
			return model;
		}
		
		static public VertexBuffer GetCube(Vector3 halfExtents)
		{
			// Dont forget it's CW. Going TR and cw
			/*float[] vertices = new float[]
			{
				// Front
				halfExtents.X, halfExtents.Y, -halfExtents.Z,   0, 0, -1,
				halfExtents.X, -halfExtents.Y, -halfExtents.Z,  0, 0, -1,
				-halfExtents.X, -halfExtents.Y, -halfExtents.Z, 0, 0, -1,
				-halfExtents.X, halfExtents.Y, -halfExtents.Z,  0, 0, -1,
				
				// Back
				halfExtents.X, halfExtents.Y, halfExtents.Z,    0, 0, 1,
				-halfExtents.X, halfExtents.Y, halfExtents.Z,   0, 0, 1,
				-halfExtents.X, -halfExtents.Y, halfExtents.Z,  0, 0, 1,
				halfExtents.X, -halfExtents.Y, halfExtents.Z,   0, 0, 1,
				
				// Left
				-halfExtents.X, halfExtents.Y, -halfExtents.Z,  -1, 0, 0,
				-halfExtents.X, -halfExtents.Y, -halfExtents.Z, -1, 0, 0,
				-halfExtents.X, -halfExtents.Y, halfExtents.Z,  -1, 0, 0,
				-halfExtents.X, halfExtents.Y, halfExtents.Z,   -1, 0, 0,
				
				// Right
				halfExtents.X, halfExtents.Y, halfExtents.Z,     1, 0, 0,
				halfExtents.X, -halfExtents.Y, halfExtents.Z,    1, 0, 0,
				halfExtents.X, -halfExtents.Y, -halfExtents.Z,   1, 0, 0,
				halfExtents.X, halfExtents.Y, -halfExtents.Z,    1, 0, 0,
				
				// Top
				halfExtents.X, halfExtents.Y, halfExtents.Z,     0, 1, 0,
				halfExtents.X, halfExtents.Y, -halfExtents.Z,     0, 1, 0,
				-halfExtents.X, halfExtents.Y, -halfExtents.Z,     0, 1, 0,
				-halfExtents.X, halfExtents.Y, halfExtents.Z,     0, 1, 0,
				
				// Bottom
				halfExtents.X, -halfExtents.Y, -halfExtents.Z,     0, -1, 0,
				halfExtents.X, -halfExtents.Y, halfExtents.Z,     0, -1, 0,
				-halfExtents.X, -halfExtents.Y, -halfExtents.Z,     0, -1, 0,
				-halfExtents.X, -halfExtents.Y, halfExtents.Z,     0, -1, 0,
			};*/
			
			float[] vertices = new float[]
			{
				// Front
				-halfExtents.X, halfExtents.Y, halfExtents.Z,   0, 0, 1,
				-halfExtents.X, -halfExtents.Y, halfExtents.Z,  0, 0, 1,
				halfExtents.X, -halfExtents.Y, halfExtents.Z, 0, 0, 1,
				halfExtents.X, halfExtents.Y, halfExtents.Z,  0, 0, 1,
				
				// Left
				-halfExtents.X, halfExtents.Y, -halfExtents.Z,  -1, 0, 0,
				-halfExtents.X, -halfExtents.Y, -halfExtents.Z, -1, 0, 0,
				-halfExtents.X, -halfExtents.Y, halfExtents.Z,  -1, 0, 0,
				-halfExtents.X, halfExtents.Y, halfExtents.Z,   -1, 0, 0,
				
				// Right
				halfExtents.X, halfExtents.Y, halfExtents.Z,     1, 0, 0,
				halfExtents.X, -halfExtents.Y, halfExtents.Z,    1, 0, 0,
				halfExtents.X, -halfExtents.Y, -halfExtents.Z,   1, 0, 0,
				halfExtents.X, halfExtents.Y, -halfExtents.Z,    1, 0, 0,
				
				// Back
				halfExtents.X, halfExtents.Y, -halfExtents.Z,    0, 0, -1,
				halfExtents.X, -halfExtents.Y, -halfExtents.Z,   0, 0, -1,
				-halfExtents.X, -halfExtents.Y, -halfExtents.Z,  0, 0, -1,
				-halfExtents.X, halfExtents.Y, -halfExtents.Z,   0, 0, -1,
				
				// Top
				-halfExtents.X, halfExtents.Y, -halfExtents.Z,     0, 1, 0,
				-halfExtents.X, halfExtents.Y, halfExtents.Z,     0, 1, 0,
				halfExtents.X, halfExtents.Y, halfExtents.Z,     0, 1, 0,
				halfExtents.X, halfExtents.Y, -halfExtents.Z,     0, 1, 0,
				
				// Bottom
				-halfExtents.X, -halfExtents.Y, halfExtents.Z,     0, -1, 0,
				-halfExtents.X, -halfExtents.Y, -halfExtents.Z,     0, -1, 0,
				halfExtents.X, -halfExtents.Y, -halfExtents.Z,     0, -1, 0,
				halfExtents.X, -halfExtents.Y, halfExtents.Z,     0, -1, 0,
			};
			
			VertexBuffer vbo = new VertexBuffer(6 * 4, 6 * 6, VertexFormat.Float3, VertexFormat.Float3);
			vbo.SetIndices(cubeIndices);
			vbo.SetVertices(vertices);
				
			return vbo;
		}
		
		static public VertexBuffer GetPyramid(Vector3 halfExtents)
		{
			float[] vertices = new float[]
			{
				// Base
				-halfExtents.X, -halfExtents.Y, halfExtents.Z,     0, -1, 0,
				-halfExtents.X, -halfExtents.Y, -halfExtents.Z,     0, -1, 0,
				halfExtents.X, -halfExtents.Y, -halfExtents.Z,     0, -1, 0,
				halfExtents.X, -halfExtents.Y, halfExtents.Z,     0, -1, 0,
				
				// Front tri
				-halfExtents.X, -halfExtents.Y, halfExtents.Z,     0, .894f, .447f,
				halfExtents.X, -halfExtents.Y, halfExtents.Z,     0, .894f, .447f,
				0, halfExtents.Y, 0,     0, .894f, .447f,
				
				// Left tri
				-halfExtents.X, -halfExtents.Y, -halfExtents.Z,     -.447f, .894f, 0,
				-halfExtents.X, -halfExtents.Y, halfExtents.Z,     -.447f, .894f, 0,
				0, halfExtents.Y, 0,     -.447f, .894f, 0,
				
				// Right tri
				halfExtents.X, -halfExtents.Y, halfExtents.Z,     .447f, .894f, 0,
				halfExtents.X, -halfExtents.Y, -halfExtents.Z,     .447f, .894f, 0,
				0, halfExtents.Y, 0,     -.447f, .894f, 0,
				
				// Back tri
				halfExtents.X, -halfExtents.Y, -halfExtents.Z,     0, .894f, -.447f,
				-halfExtents.X, -halfExtents.Y, -halfExtents.Z,     0, .894f, -.447f,
				0, halfExtents.Y, 0,     0, .894f, -.447f,
				
			};
			
			VertexBuffer vbo = new VertexBuffer(4 + 3 * 4, 6 + 3 * 4, VertexFormat.Float3, VertexFormat.Float3);
			vbo.SetIndices(pyramidIndices);
			vbo.SetVertices(vertices);
			
			return vbo;
		}
		
		static public Mesh GetSphere(float radius)
		{
			return MeshUtility.GetMeshObj("/Application/models/sphere.obj", true, radius / .5f);
		}
		
		static public void GetPlane()
		{
			
		}
	}
}

