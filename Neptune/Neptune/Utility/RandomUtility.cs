using System;

namespace Neptune.Utility
{
	public class RandomUtility
	{
		static public double MinMaxRandomDouble(double minimum, double maximum) { 
            Random random = new Random();
            return random.NextDouble() * (maximum - minimum) + minimum;
        }

        static public int MinMaxRandomInt(int minimum, int maximum) {
            Random random = new Random();
            return (int)(random.NextDouble() * ((maximum + 1) - minimum) + minimum);
        }
	}
}

