using System;

namespace Neptune
{
	public class Joystick {
		public float Angle;
		public float X;
		public float Y;
		
		public Joystick() {
			Angle = 0.0f;
			X = 0.0f;
			Y = 0.0f;
		}
		
		public void Update(float x, float y)
		{
			X = x;
			Y = y;
			Angle = (float)Math.Atan2(y, x);
		}
	}
}

