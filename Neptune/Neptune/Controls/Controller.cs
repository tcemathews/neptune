using System;
using Sce.PlayStation.Core.Input;

namespace Neptune.Controls
{
	public enum Buttons :uint {
		Up = GamePadButtons.Up,
		Right = GamePadButtons.Right,
		Down = GamePadButtons.Down,
		Left = GamePadButtons.Left,
		Btn1 = GamePadButtons.Cross,
		Btn2 = GamePadButtons.Circle,
		Btn3 = GamePadButtons.Square,
		Btn4 = GamePadButtons.Triangle,
		ShoulderRight = GamePadButtons.R,
		ShoulderLeft = GamePadButtons.L
	}
	
	public class Controller
	{
		public byte Index;
		public GamePadData GamePadData;
		public Joystick JoystickLeft;
		public Joystick JoystickRight;
		
		public Controller(byte controllerIndex)
		{
			Index = controllerIndex;
			JoystickLeft = new Joystick();
			JoystickRight = new Joystick();
		}
		
		public void Update()
		{
			GamePadData = GamePad.GetData(Index);
			JoystickLeft.Update(GamePadData.AnalogLeftX, GamePadData.AnalogLeftY);
			JoystickRight.Update(GamePadData.AnalogRightX, GamePadData.AnalogRightY);
		}
		
		public bool Pressed(Buttons button)
		{
			GamePadButtons key = (GamePadButtons)button;
			return (GamePadData.ButtonsDown & key) == key;
		}
		
		public bool Released(Buttons button)
		{
			GamePadButtons key = (GamePadButtons)button;
			return (GamePadData.ButtonsUp & key) == key;
		}
		
		public bool Held(Buttons button)
		{
			GamePadButtons key = (GamePadButtons)button;
			return (GamePadData.ButtonsPrev & key) == key;
		}
	}
}

