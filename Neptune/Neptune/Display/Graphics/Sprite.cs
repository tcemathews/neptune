using System;
using System.Collections.Generic;
using Neptune.Display.Rendering;

namespace Neptune.Display.Graphics
{
	public class SpriteAnimation {
		public string[] Frames;
		public double Interval;
		
		public SpriteAnimation (string[] frames, double interval) 	
		{
			Frames = frames;
			Interval = interval;
		}
	}
	
	public class Sprite : Graphic
	{
		public Dictionary<string, SpriteAnimation> Animations;
		public SpriteAnimation Animation;
		public double Lapse;
		public int Index;
		public bool IsPlaying;
		
		public Sprite ()
		{
			Animations = new Dictionary<string, SpriteAnimation>();
			Animation = null;
			Lapse = 0.0f;
			Index = 0;
			IsPlaying = false;
		}
		
		public void AddAnimation(string key, string[] frames, double interval)
		{
			if ( frames.Length == 0 || key == null ) {
				throw new Exception("Invalid parameters.");	
			}
			Animations.Add(key, new SpriteAnimation(frames, interval));
		}
		
		public string GetDisplayingFrameTextureKey() 
		{
			if ( Animation != null ) {
				return Animation.Frames[Index];	
			}
			return null;
		}
		
		public int GetFrameIndex()
		{
			return Index;
		}
		
		public void SetFrameIndex(int index)
		{
			if ( Animation != null && index < Animation.Frames.Length && index >= 0 ) {
				Index = index;
				IsPlaying = false;
			}
		}
		
		public void Play(string key)
		{
			if ( Animations.TryGetValue(key, out Animation) ) {
				Index = 0;
				Lapse = 0.0;
			}
			else {
				Animation = null;
			}
		}
		
		public void Stop()
		{
			IsPlaying = false;
		}
		
		public void Update(double dt)
		{
			if ( IsPlaying && Animation != null ) {
				Lapse += dt;
				while ( Lapse > Animation.Interval ) {
					Index++;
					if ( Index >= Animation.Frames.Length ) {
						Index = 0;
					}
					Lapse -= Animation.Interval;
				}
			}
		}
	}
}
