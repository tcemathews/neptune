using System;
using Sce.PlayStation.Core;
using Neptune.Display.Rendering;

namespace Neptune.Display.Graphics
{
	public class Picture : Graphic
	{
		public string TextureKey;
		
		public Picture (string textureKey)
		{
			TextureKey = textureKey;
		}
	}
}

