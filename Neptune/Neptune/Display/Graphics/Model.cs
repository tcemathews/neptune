using Neptune.Display.Rendering;

namespace Neptune.Display.Graphics
{
	public class Model : Graphic
	{
		public string TextureKey;
		public string MeshKey;
		
		public Model(string texturekey, string meshkey)
		{
			TextureKey = texturekey;
			MeshKey = meshkey;
			Dimensions = 3;
		}
		
		public void Play(string key)
		{
			// Plays animation from the model data.	
		}
		
		public void SetFrame()
		{
			// Sets model to state in model data.
		}
		
		public void Update(double dt)
		{
			// Updates the model animation.
		}
	}
}

