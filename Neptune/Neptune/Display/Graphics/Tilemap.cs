using System;
using Sce.PlayStation.Core;
using Neptune.Display.Rendering;

namespace Neptune.Display.Graphics
{
	public class Tilemap : Graphic
	{
		public string TextureKey;
		public Vector2 TileSize; // size of one tile in pixels
		public Vector2 MapSize;  // size of map in tiles.
		public int[] Map;
		
		public Tilemap(string texturekey, int tilewidth, int tileheight, int mapwidth, int mapheight)
		{
			TextureKey = texturekey;
			TileSize = new Vector2(tilewidth, tileheight);
			MapSize = new Vector2(mapwidth, mapheight);
			Map = new int[(int)MapSize.X * (int)MapSize.Y];
			
			for (int i = 0;  i < Map.Length; ++i) {
				Map[i] = 0;
			}
		}
		
		public void SetTile(int tileid, int mapindex)
		{
			Map[mapindex] = tileid;
		}
		
		public int GetTile(int mapindex)
		{
			if ( mapindex >= Map.Length || mapindex < 0 ) return -1;
			return Map[mapindex];
		}
		
		public int GetTileFromXy(int x, int y) {
			return GetTile((int)((int)(y / TileSize.Y) * MapSize.X) + (int)(x / TileSize.X));
		}
		
		public Vector2 GetXyFromMapIndex(int mapindex)
		{
			int x = (int)(mapindex % MapSize.X);
			int y = (int)(mapindex / MapSize.X);
			return new Vector2(x * TileSize.X, y * TileSize.Y);
		}
	}
}