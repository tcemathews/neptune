using System;
using Sce.PlayStation.Core;
using Neptune.Display.Rendering;

namespace Neptune.Display.Graphics
{
	public enum LabelAlignment {
		Left = 0,
		Center = 1,
		Right = 2
	}
	
	public class Label : Graphic
	{
		public string FontName; // The name of the preloaded font we want to use to render.
		public string Text; // The text to be printed.
		public Rectangle ClipFrame; // The frame to wrap and clip text with.
		public bool Wraps; // Determins if the text wraps to the next line if it clips through the width.
		public LabelAlignment Alignment; // Determins the alignment of the text to be rendering.
		
		public Label(string fontname)
		{
			FontName = fontname;
			Text = "";
			ClipFrame = new Rectangle(0, 0, 256, 256);
			Wraps = true;
			Alignment = LabelAlignment.Left;
		}
	}
}