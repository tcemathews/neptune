using System;
using System.Collections.Generic;
using Sce.PlayStation.Core;

namespace Neptune.Display.Atlas
{
	public class FontCharacter
	{
		public int X;
		public int Y;
		public int Width;
		public int Height;
		public int XOffset;
		public int YOffset;
		public int XAdvance;
		
		public FontCharacter(int x, int y, int width, int height, int xoffset, int yoffset, int xadvance)
		{
			this.X = x;
			this.Y = y;
			this.XOffset = xoffset;
			this.YOffset = yoffset;
			this.Width = width;
			this.Height = height;
			this.XAdvance = xadvance;
		}
	}
	
	public class FontAtlas
	{
		public string TextureKey;
		public Dictionary<int, FontCharacter> CharacterMap;
		public int LineHeight;
		public int Size;
		
		public FontAtlas(string textureKey, int size, int lineHeight, Dictionary<int, FontCharacter> map)
		{
			TextureKey = textureKey;
			CharacterMap = map;
			Size = size;
			LineHeight = lineHeight;
		}
	}
}
