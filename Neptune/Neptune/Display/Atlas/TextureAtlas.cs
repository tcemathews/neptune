using System;
using System.Collections.Generic;
using Sce.PlayStation.Core;

namespace Neptune.Display.Atlas
{
	public class TextureAtlas
	{
		public string TextureFile;
		public Dictionary<string, Rectangle> Atlas;
		
		public TextureAtlas(string textureFile, Dictionary<string, Rectangle> atlas)
		{
			TextureFile = textureFile;
			Atlas = atlas;
		}
	}
}

