using System;
using Sce.PlayStation.Core;

namespace Neptune.Display
{
	public class Color
	{
		public Vector4 Vector;
		
		public Color(byte red = 255, byte green = 255, byte blue = 255, byte alpha = 255 )
		{
			Vector = new Vector4(red, green, blue, alpha);
		}
		
		public void SetRGB(byte red, byte green, byte blue) 
		{
			Vector.X = red;
			Vector.Y = green;
			Vector.Z = blue;
			Vector.W = 255;
		}
		
		public void SetRGBA(byte red, byte green, byte blue, byte alpha)
		{
			Vector.X = red;
			Vector.Y = green;
			Vector.Z = blue;
			Vector.W = alpha;
		}
		
		public void SetAlpha(byte alpha )
		{
			Vector.W = alpha;
		}
	}
}

