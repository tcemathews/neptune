using Sce.PlayStation.Core;

namespace Neptune.Display.Rendering
{
	public class Camera
	{
		public Vector3 Position;
		
		public Camera()
		{
			Position = new Vector3(0, 0, 5.0f);
		}
	}
}
