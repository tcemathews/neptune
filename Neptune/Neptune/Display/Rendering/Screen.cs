using System.Collections.Generic;
using Neptune.Display;
using Neptune.Display.Rendering;

namespace Neptune.Display.Rendering
{
	public class Screen
	{
		public int Width;
		public int Height;
		public Color Color;
		public Lighting Lighting;
		public List<Graphic> Graphics;
		public List<Graphic> Graphics3D;
		public Cache Cache;
		public Camera Camera;
		
		public Screen()
		{
			Width = 0;
			Height = 0;
			Color = new Color(0, 0, 0, 255);
			Lighting = new Lighting();
			Graphics = new List<Graphic>();
			Graphics3D = new List<Graphic>();
			Cache = new Cache();
			Camera = new Camera();
		}
		
		public void SortGraphics()
		{
			Graphics.Sort();
		}
		
		public void AddGraphic(Graphic graphic)
		{
			List<Graphic> list = Graphics;
			if (graphic.Dimensions == 3) {
				list = Graphics3D;
			}
			
			if (list.Count == 0 || list[list.Count - 1].Z < graphic.Z) {
				list.Add (graphic);
			}
			else
			{
				for (int i = 0; i < list.Count; ++i)
				{
					if (graphic.Z <= list[i].Z) {
						list.Insert(i, graphic);
						break;
					}
				}
			}
			graphic.InStack = true;
		}
		
		public void RemoveGraphic(Graphic graphic)
		{
			Graphics.Remove(graphic);
			Graphics3D.Remove(graphic);
			graphic.InStack = false;
		}
	}
}
