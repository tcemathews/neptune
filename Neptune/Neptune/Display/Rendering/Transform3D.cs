using System;

using Sce.PlayStation.Core;

namespace Neptune.Display.Rendering
{
	public class Transform3D
	{
		public Vector3 Rotation;
		
		public Transform3D ()
		{
			Rotation = new Vector3(0.0f);
		}
	}
}

