using System;

// http://community.eu.playstation.com/t5/General/Abysmal-Vita-CPU-performance/m-p/18814450/highlight/true#M4051

using Sce.PlayStation.Core;
using Sce.PlayStation.Core.Graphics;
using Sce.PlayStation.Core.Imaging;

using Neptune.Display.Rendering;
using Neptune.Display.Graphics;
using Neptune.Display.Atlas;
using Neptune.Utility;

namespace Neptune.Display.Rendering
{
	public  class ExperimentalWaves
	{
		// http://www.jayconrod.com/posts/34/water-simulation-in-glsl
		// http://www.jayconrod.com/posts/50/water-simulation-demo-in-webgl **
		// http://codeflow.org/entries/2011/nov/10/webgl-gpu-landscaping-and-erosion/
		// http://www.cornflex.org/?p=1019
		// http://www.alcove-games.com/opengl-es-2-tutorials/vertex-shader-for-tiled-water/
		// http://users.csc.calpoly.edu/~zwood/teaching/csc471/finalproj010s2/cgilson/
		// http://rotatingcanvas.com/fragment-shader-to-simulate-water-surface-in-libgdx/ 2d side view shaders
		// http://forum.unity3d.com/threads/34031-Generating-Water-Textures-with-Perlin-Noise
		
		// Width in 'cells' number of points across would be gridX + 1
		static readonly int gridWidth = 100;
		static readonly int gridHeight = 100;
		static readonly float CellDimensions = .5f;
		
		public VertexBuffer vbo;
		
		static readonly int FLOATS_PER_VERTEX  = 6;
		static readonly int UNIQUE_VERTICES_PER_CELL  = 4; // Since duplicating in vboVertices - is 4 in gridPointHeights
		static readonly int INDICES_PER_CELL = 6;
		
		float [] vboVertices;
		float [] gridPointHeights;
		ushort [] indices;
		ShaderProgram waveShader;
		
		int numWaves;
		
		float [] wavelengths = new float[]{ .1415927f, 6 };
		Vector2 [] directions = new Vector2[] { new Vector2(-1.5f, .5f), new Vector2(0, 5f) };
		float [] amplitudes = new float[]{ .25f, 1.0f };
		float [] speeds = new float [] { .1f, .5f };
		float time = 0;
		
		int GetVertexIndex(int whichQuad, int whichVertex)
		{
			return FLOATS_PER_VERTEX * UNIQUE_VERTICES_PER_CELL * whichQuad + whichVertex * FLOATS_PER_VERTEX;
		}
		
		public ExperimentalWaves()
		{
			numWaves = wavelengths.Length;
			
			vboVertices = new float[gridWidth * gridHeight * FLOATS_PER_VERTEX * UNIQUE_VERTICES_PER_CELL];
			gridPointHeights = new float [(gridHeight + 1) * (gridWidth + 1)]; // keeps a height
			indices = new ushort[6 * gridWidth * gridHeight];
			
			int numFaces = gridWidth * gridHeight;
			for (int i = 0; i < numFaces; ++i)
			{
				int iIndex = i * 6;
				int vIndex = i * 4;
				
				indices[iIndex++] = (ushort)(vIndex);
				indices[iIndex++] = (ushort)(vIndex + 1);
				indices[iIndex++] = (ushort)(vIndex + 2);
				
				indices[iIndex++] = (ushort)(vIndex);
				indices[iIndex++] = (ushort)(vIndex + 2);
				indices[iIndex++] = (ushort)(vIndex + 3);
				
			}
			
			vbo = new VertexBuffer(gridWidth * gridHeight * UNIQUE_VERTICES_PER_CELL, gridWidth * gridHeight * INDICES_PER_CELL, 
			                       VertexFormat.Float3, VertexFormat.Float3);
			vbo.SetVertices(vboVertices);
			vbo.SetIndices(indices);
			
			waveShader = new ShaderProgram("/Application/shaders/TestWater.cgx");
			waveShader.SetUniformBinding(0, "uMVP");
		}
		
		public void Draw(GraphicsContext context)
		{
			context.SetShaderProgram(waveShader);
			
			context.SetVertexBuffer(0, vbo);
			
			Matrix4 view = Matrix4.LookAt(new Vector3(0, 2, 4), new Vector3(0, 0, 0), new Vector3(0, 1, 0));
			Matrix4 world = Matrix4.Translation(0, 0, -3);
			Matrix4 mvp = Matrix4.Perspective(45 * 3.1415927f/180.0f, context.Screen.Width/(float)context.Screen.Height, .25f, 50.0f) *
				view * world;
			
			waveShader.SetUniformValue(0, ref mvp);
			
			context.DrawArrays(DrawMode.Triangles, 0, gridWidth * gridHeight * 6);
		}
		
		private float Wave(int wave, float x, float y)
		{
			float frequency = 2 * 3.1415927f / wavelengths[wave];
			float phase = speeds[wave] * frequency;
			float theta = directions[wave].Dot(new Vector2(x, y));
			return amplitudes[wave] * FMath.Sin(theta * frequency + time * phase);
		}
		
		public float GetWaveHeight(float x, float y)
		{
			float height = 0;
			for (int i = 0; i < numWaves; ++i)
			{
				height += Wave(i, x, y);
			}
			
			// Just some offsetting
			height *= CellDimensions;
			height -= 9.0f;
			
			return height;
		}
		
		float dWavedx(int i, float x, float y) {
		    float frequency = 2*3.1415927f/wavelengths[i];
		    float phase = speeds[i] * frequency;
		    float theta = directions[i].Dot(new Vector2(x, y));
		    float A = amplitudes[i] * directions[i].X * frequency;
		    return A * (float)Math.Cos(theta * frequency + time * phase);
		}
		
		float dWavedy(int i, float x, float y) {
		    float frequency = 2 * 3.14159247f / wavelengths[i];
		    float phase = speeds[i] * frequency;
		    float theta = directions[i].Dot(new Vector2(x, y));
		    float A = amplitudes[i] * directions[i].Y * frequency;
		    return A * (float)Math.Cos(theta * frequency + time * phase);
		}
		
		Vector3 WaveNormal(float x, float y) {
		    float dx = 0.0f;
		    float dy = 0.0f;
		    for (int i = 0; i < numWaves; ++i) {
		        dx += dWavedx(i, x, y);
		        dy += dWavedy(i, x, y);
		    }
		    
			// Vector3 n = new Vector3(-dx, -dy, 1.0f);
			Vector3 n = new Vector3(-dx, 1.0f, dy);
			
		    return n.Normalize();
		}
		
		// x, y are a whole point (NOT a cell)
		float GetHeightAtPoint(int x, int z)
		{
			return gridPointHeights[z * (gridWidth + 1) + x];
		}
		
		public void CalculateVertices(float dt)
		{
			time += dt * .25f;
			
			for (int z = 0; z < gridHeight; ++z)
			{
				for (int x = 0; x < gridWidth; ++x)
				{
					gridPointHeights[z * (gridWidth + 1) + x] = GetWaveHeight(x, z);
					gridPointHeights[z * (gridWidth + 1) + x + 1] = GetWaveHeight(x + 1, z);
					gridPointHeights[(z + 1) * (gridWidth + 1) + x] = GetWaveHeight(x, z + 1);
					gridPointHeights[(z + 1) * (gridWidth + 1) + x + 1] = GetWaveHeight(x + 1, z + 1);
				}
			}
			
			float dimensions = CellDimensions;
			float offsetX = -10;
			float offsetZ = -40;
			Vector3 normal;
				
			for (int z = 0; z < gridHeight; ++z)
			{
				for (int x = 0; x < gridWidth; ++x)
				{
					int vboVertIndex = FLOATS_PER_VERTEX * UNIQUE_VERTICES_PER_CELL * (z * gridWidth + x);
					
					// normal = WaveNormal(x, z);
					Vector3 a = new Vector3((x * dimensions) + offsetX, gridPointHeights[z * (gridWidth + 1) + x], z * dimensions + offsetZ);
					Vector3 b = new Vector3(x * dimensions + offsetX, gridPointHeights[(z + 1) * (gridWidth + 1) + x], (z + 1) * dimensions + offsetZ);
					Vector3 line1 = (a - b).Normalize();
					a = new Vector3((x + 1) * dimensions + offsetX, gridPointHeights[z * (gridWidth + 1) + x + 1], z * dimensions + offsetZ);
					Vector3 line2 = (a - b).Normalize();
					
					normal = line2.Cross(line1);
					
					//normal = WaveNormal(x, z);
					
					// tl
					vboVertices[vboVertIndex++] = (x * dimensions) + offsetX;
					vboVertices[vboVertIndex++] = GetHeightAtPoint(x, z);
					vboVertices[vboVertIndex++] = z * dimensions + offsetZ;
					vboVertices[vboVertIndex++] = normal.X;
					vboVertices[vboVertIndex++] = normal.Y;
					vboVertices[vboVertIndex++] = normal.Z;
					//vboVertIndex += 3; // FOr the normal
					
					// bl
					vboVertices[vboVertIndex++] = x * dimensions + offsetX;
					vboVertices[vboVertIndex++] = GetHeightAtPoint(x, z + 1);
					vboVertices[vboVertIndex++] = (z + 1) * dimensions + offsetZ;
					vboVertices[vboVertIndex++] = normal.X;
					vboVertices[vboVertIndex++] = normal.Y;
					vboVertices[vboVertIndex++] = normal.Z;
					//vboVertIndex += 3; // FOr the normal
					
					// br
					vboVertices[vboVertIndex++] = (x + 1) * dimensions + offsetX;
					vboVertices[vboVertIndex++] = GetHeightAtPoint(x + 1, z + 1);
					vboVertices[vboVertIndex++] = (z + 1) * dimensions + offsetZ;
					//vboVertIndex += 3; // FOr the normal
					vboVertices[vboVertIndex++] = normal.X;
					vboVertices[vboVertIndex++] = normal.Y;
					vboVertices[vboVertIndex++] = normal.Z;
					
					// tr
					vboVertices[vboVertIndex++] = (x + 1) * dimensions + offsetX;
					vboVertices[vboVertIndex++] = GetHeightAtPoint(x + 1, z);
					vboVertices[vboVertIndex++] = z * dimensions + offsetZ;
					//vboVertIndex += 3; // FOr the normal
					vboVertices[vboVertIndex++] = normal.X;
					vboVertices[vboVertIndex++] = normal.Y;
					vboVertices[vboVertIndex++] = normal.Z;
				}
			}
			
			vbo.SetVertices(vboVertices);
		}
	}
	
	public class Render
	{
		public static readonly int MAX_SPRITES = 2048;
		
		public GraphicsContext Context;
		public ShaderProgram   SpriteShader;
		public VertexBuffer    SpriteVBO;
		float[]                SpriteVertices;
		
		public ShaderProgram   ScatterShader;
		public ShaderProgram   VelvetShader;
		
		// No allocations!     
		private Vector4        TempVec4;
		
		// Bindings
		private readonly int UNIFORM_MVP = 0;
		private readonly int UNIFORM_MODELVIEW = 1;
		private readonly int UNIFORM_COLOR = 2;
		
		public readonly static int FLOATS_PER_SPRITE_VERTEX = 8;
		public Render(GraphicsContext gc)
		{
			Context = gc;
			SpriteShader = new ShaderProgram("/Application/shaders/Sprite.cgx");
			
			ScatterShader = new ShaderProgram("/Application/shaders/Scatter.cgx");
			ScatterShader.SetUniformBinding(UNIFORM_MVP, "uMVP");
			ScatterShader.SetUniformBinding(UNIFORM_MODELVIEW, "uModelview");
			ScatterShader.SetUniformBinding(UNIFORM_COLOR, "uColor");
			
			VelvetShader = new ShaderProgram("/Application/shaders/Velvet.cgx");
			VelvetShader.SetUniformBinding(UNIFORM_MVP, "uMVP");
			VelvetShader.SetUniformBinding(UNIFORM_MODELVIEW, "uModelview");
			VelvetShader.SetUniformBinding(UNIFORM_COLOR, "uColor");
			
			// 'align' format to order specified in shader
			SpriteVBO = new VertexBuffer(4 * MAX_SPRITES, 6 * MAX_SPRITES, VertexFormat.Float2, VertexFormat.Float2, VertexFormat.Float4);
			
			ushort [] spriteIndices = new ushort[MAX_SPRITES * 6];
			for (int i = 0; i < MAX_SPRITES; ++i)
			{
				int index = i * 6;
				
				spriteIndices[index + 0] = (ushort)(i * 4 + 0);
				spriteIndices[index + 1] = (ushort)(i * 4 + 1);
				spriteIndices[index + 2] = (ushort)(i * 4 + 2);
				
				spriteIndices[index + 3] = (ushort)(i * 4 + 0);
				spriteIndices[index + 4] = (ushort)(i * 4 + 2);
				spriteIndices[index + 5] = (ushort)(i * 4 + 3);
			}
			
			SpriteVBO.SetIndices(spriteIndices);
			
			SpriteVertices = new float[MAX_SPRITES * 4 * FLOATS_PER_SPRITE_VERTEX];
			for (int i = 0; i < SpriteVertices.Length; ++i) {
				SpriteVertices[i] = 1.0f;
			}
			
			SpriteVBO.SetVertices(SpriteVertices);
 
			TempVec4 = new Vector4(0, 0, 0, 0);
		}
		
		ExperimentalWaves waves = new ExperimentalWaves();
		
		public void Update3D(Screen screen)
		{
			ShaderProgram shader = ScatterShader;
			Context.SetShaderProgram(shader);
			
			Context.Enable(EnableMode.CullFace);
			Context.Enable(EnableMode.DepthTest);
			
			Matrix4 mvp;
			Matrix4 world;
			Matrix4 view = Matrix4.LookAt(screen.Camera.Position, new Vector3(0, 0, 0), new Vector3(0, 1.0f, 0));
			Matrix4 projection = Matrix4.Perspective(45 * 3.1415927f/180.0f, Context.Screen.Width/(float)Context.Screen.Height, .25f, 50.0f);
			
			for (int i = 0; i < screen.Graphics3D.Count; ++i)
			{
				Graphic graphic = screen.Graphics3D[i];
				if (graphic.Visible)
				{
					if (graphic.GetType() == typeof(Model))
					{
						Model model = (Model)graphic;
						Mesh meshCollection = screen.Cache.StaticMeshes[model.MeshKey];
						                                           
						VertexBuffer vbo = meshCollection.VertexBuffer;
						
						world = Matrix4.Translation(graphic.X, graphic.Y, graphic.Z) *
							 Matrix4.RotationX(3.1415927f / 180.0f * graphic.Transform3D.Rotation.X) * 
								Matrix4.RotationY(3.1415927f / 180.0f * model.Transform3D.Rotation.Y);
						
						mvp = projection * view * world; // Weird using this order works
						
						shader.SetUniformValue(UNIFORM_MVP, ref mvp);
						shader.SetUniformValue(UNIFORM_MODELVIEW, ref world);
						Context.SetVertexBuffer(0, vbo);
						
						Vector4 color = new Vector4(0, 1.0f, 0, 1.0f);
						for (int submeshLoop = 0; submeshLoop < meshCollection.MeshDescriptions.Count; ++submeshLoop)
						{
							Mesh.MeshDesc desc = meshCollection.MeshDescriptions[submeshLoop];
							
							shader.SetUniformValue(UNIFORM_COLOR, ref desc.Color);
							Context.DrawArrays(DrawMode.Triangles, desc.Offset, desc.Count);
						}
						
						// Context.DrawArrays(DrawMode.Triangles, 0, vbo.IndexCount);
					}
				}
			}
			
			// waves.CalculateVertices(1/60.0f);
			// waves.Draw(Context); //
		}
		
		public void Update(Screen screen)
		{
			Context.SetClearColor(screen.Color.Vector);
			Context.Clear(ClearMask.All);
			
			Update3D(screen);
			Update2D(screen);
			
			Context.SwapBuffers();
		}
		
		public void Update2D(Screen screen)
		{
			Context.Disable(EnableMode.DepthTest);
			Context.Enable(EnableMode.Blend);
			Context.SetBlendFunc(BlendFuncMode.Add, BlendFuncFactor.SrcAlpha, BlendFuncFactor.OneMinusSrcAlpha);
			
			Context.SetShaderProgram(SpriteShader);
			Matrix4 projection = Matrix4.Ortho(0, Context.Screen.Width, Context.Screen.Height, 0, -1, 1);
			
			SpriteShader.SetUniformValue(0, ref projection);

			int numSprites = 0;
			for (int i = 0; i < screen.Graphics.Count; ++i)
			{
				Graphic graphic = screen.Graphics[i];
				if (graphic.Visible)
				{
					if (graphic.GetType() == typeof(Sprite) || graphic.GetType() == typeof(Picture))
					
					{
						Rectangle rect = new Rectangle(0, 0, 1, 1);
						
						if (graphic.GetType() == typeof(Sprite)) {
							string key = ((Sprite)graphic).GetDisplayingFrameTextureKey();
							if (key != null) {
								rect = screen.Cache.GetRectangleForTextureKey(key);
							}
						}
						else {
							rect = screen.Cache.GetRectangleForTextureKey(((Picture)graphic).TextureKey);
						}
						
						TempVec4.X = graphic.Transform2D.Color.Vector.R / 255.0f;
						TempVec4.Y = graphic.Transform2D.Color.Vector.G / 255.0f;
						TempVec4.Z = graphic.Transform2D.Color.Vector.B / 255.0f;
						TempVec4.W = graphic.Transform2D.Color.Vector.A / 255.0f;
						
						int floatIndex = numSprites * 4 * FLOATS_PER_SPRITE_VERTEX;
						
						for (int j = 0; j < 4; ++j)
						{
							int index = floatIndex + j * FLOATS_PER_SPRITE_VERTEX;
							SpriteVertices[index + 4] = TempVec4.X;
							SpriteVertices[index + 5] = TempVec4.Y;
							SpriteVertices[index + 6] = TempVec4.Z;
							SpriteVertices[index + 7] = TempVec4.W;
						}
						
						// Normally would put in a function but worried about function overhead on slow vm
						// http://www.gamedev.net/topic/382128-2d-rotation-around-an-arbitrary-point/
						float x;
						float y;
						
						float width = rect.Width;
						float height = rect.Height;
						float positionX = graphic.X - (-.5f + graphic.Transform2D.Origin.X) * width;
						float positionY = graphic.Y - (-.5f + graphic.Transform2D.Origin.Y) * height;
						float halfWidth = rect.Width * .5f;
						float halfHeight = rect.Height * .5f;
					//	graphic.Transform2D.Rotation = 45.0f * 3.1415927f / 180.0f;
						float cosTheta = FMath.Cos(graphic.Transform2D.Rotation);
						float sinTheta = FMath.Sin(graphic.Transform2D.Rotation);
						
						float pivotDX = - (-.5f + graphic.Transform2D.Origin.X) * width; 
						float pivotDY = - (-.5f + graphic.Transform2D.Origin.Y) * height;
						
						x = (-halfWidth + pivotDX) * graphic.Transform2D.Scale.X;
						y = (-halfHeight + pivotDY) * graphic.Transform2D.Scale.Y;
						SpriteVertices[floatIndex] = cosTheta * x - sinTheta * y + positionX - pivotDX;
						SpriteVertices[floatIndex + 1] = sinTheta * x + cosTheta * y + positionY - pivotDY;
						SpriteVertices[floatIndex + 2] = rect.X / (float)screen.Cache.Size;
						SpriteVertices[floatIndex + 3] = rect.Y / (float)screen.Cache.Size;
						
						floatIndex += FLOATS_PER_SPRITE_VERTEX;
						x = (-halfWidth + pivotDX) * graphic.Transform2D.Scale.X;
						y = (halfHeight + pivotDY) * graphic.Transform2D.Scale.Y;
						SpriteVertices[floatIndex] = cosTheta * x - sinTheta * y + positionX - pivotDX;
						SpriteVertices[floatIndex + 1] = sinTheta * x + cosTheta * y + positionY - pivotDY;
						SpriteVertices[floatIndex + 2] = rect.X / (float)screen.Cache.Size;
						SpriteVertices[floatIndex + 3] = (rect.Y + rect.Height) / (float)screen.Cache.Size;
						
						floatIndex += FLOATS_PER_SPRITE_VERTEX;
						x = (halfWidth + pivotDX) * graphic.Transform2D.Scale.X;
						y = (halfHeight + pivotDY) * graphic.Transform2D.Scale.Y;
						SpriteVertices[floatIndex] = cosTheta * x - sinTheta * y + positionX - pivotDX;
						SpriteVertices[floatIndex + 1] = sinTheta * x + cosTheta * y + positionY - pivotDY;
						SpriteVertices[floatIndex + 2] = (rect.X + rect.Width) / (float)screen.Cache.Size;
						SpriteVertices[floatIndex + 3] = (rect.Y + rect.Height) / (float)screen.Cache.Size;
						
						floatIndex += FLOATS_PER_SPRITE_VERTEX;
						x = (halfWidth + pivotDX) * graphic.Transform2D.Scale.X;
						y = (-halfHeight + pivotDY) * graphic.Transform2D.Scale.Y;
						SpriteVertices[floatIndex] = cosTheta * x - sinTheta * y + positionX - pivotDX;
						SpriteVertices[floatIndex + 1] = sinTheta * x + cosTheta * y + positionY - pivotDY;
						SpriteVertices[floatIndex + 2] = (rect.X + rect.Width) / (float)screen.Cache.Size;
						SpriteVertices[floatIndex + 3] = (rect.Y) / (float)screen.Cache.Size;
						
						// Console.WriteLine("Foo " + graphic.X + " " + graphic.Y + " " + rect.Width + " " + rect.Height + " " + manager.CubeTexture.Size);
						++numSprites;
					}
					else if (graphic.GetType() == typeof(Tilemap))
					{
						Tilemap tilemap = (Tilemap)graphic;
						Rectangle textureRect = screen.Cache.GetRectangleForTextureKey(tilemap.TextureKey);
						
						int tilesWide = (int)(textureRect.Width / tilemap.TileSize.X);
						int tilesHigh = (int)(textureRect.Height / tilemap.TileSize.Y);
						
						float du = tilemap.TileSize.X / (float)screen.Cache.LastTexture.Width;
						float dv = tilemap.TileSize.Y / (float)screen.Cache.LastTexture.Height;
						
						int floatIndex = numSprites * 4 * FLOATS_PER_SPRITE_VERTEX;
						
						for (int y = 0; y < tilemap.MapSize.Y; ++y)
						{
							for (int x = 0; x < tilemap.MapSize.X; ++x)
							{
								int tile = tilemap.Map[(int)(tilemap.MapSize.X * y + x)];
								int tileX = tile % tilesWide;
								int tileY = tile / tilesWide;
								
								float u = (textureRect.X + tileX * tilemap.TileSize.X) / (float)screen.Cache.LastTexture.Width;
								float v = (textureRect.Y + tileY * tilemap.TileSize.Y) / (float)screen.Cache.LastTexture.Height;
								
								SpriteVertices[floatIndex++] = tilemap.X + x * tilemap.TileSize.X;
								SpriteVertices[floatIndex++] = tilemap.Y + y * tilemap.TileSize.Y;
								SpriteVertices[floatIndex++] = u;
								SpriteVertices[floatIndex++] = v;
								floatIndex += 4; // Skip color
								
								SpriteVertices[floatIndex++] = tilemap.X + x * tilemap.TileSize.X;
								SpriteVertices[floatIndex++] = tilemap.Y + y * tilemap.TileSize.Y + tilemap.TileSize.Y;
								SpriteVertices[floatIndex++] = u;
								SpriteVertices[floatIndex++] = v + dv;
								floatIndex += 4;
								
								SpriteVertices[floatIndex++] = tilemap.X + x * tilemap.TileSize.X + tilemap.TileSize.X;
								SpriteVertices[floatIndex++] = tilemap.Y + y * tilemap.TileSize.Y + tilemap.TileSize.Y;
								SpriteVertices[floatIndex++] = u + du;
								SpriteVertices[floatIndex++] = v + dv;
								floatIndex += 4;
								
								SpriteVertices[floatIndex++] = tilemap.X + x * tilemap.TileSize.X + tilemap.TileSize.X;
								SpriteVertices[floatIndex++] = tilemap.Y + y * tilemap.TileSize.Y;
								SpriteVertices[floatIndex++] = u + du;
								SpriteVertices[floatIndex++] = v;
								floatIndex += 4;
								
								++numSprites;
							}
						}
					}
					else if (graphic.GetType() == typeof(Label))
					{
						Label label = (Label)graphic;
						
						// Description of character -> rectangle with offset and advance while rendering
						FontAtlas fontAtlas = screen.Cache.Fonts[label.FontName];
						
						// The txture block that contains all characters within the greater cube texture
						Rectangle textureRect = screen.Cache.GetRectangleForTextureKey(fontAtlas.TextureKey);
						
						int floatIndex = numSprites * 4 * FLOATS_PER_SPRITE_VERTEX;
						
						/*SpriteVertices[floatIndex++] = 0;
						SpriteVertices[floatIndex++] = 0;
						SpriteVertices[floatIndex++] = 0;
						SpriteVertices[floatIndex++] = 0;
						floatIndex += 4;
						
						SpriteVertices[floatIndex++] = 0;
						SpriteVertices[floatIndex++] = 256;
						SpriteVertices[floatIndex++] = 0;
						SpriteVertices[floatIndex++] = 1;
						floatIndex += 4;
						
						SpriteVertices[floatIndex++] = 256;
						SpriteVertices[floatIndex++] = 256;
						SpriteVertices[floatIndex++] = 1;
						SpriteVertices[floatIndex++] = 1;
						floatIndex += 4;
						
						SpriteVertices[floatIndex++] = 256;
						SpriteVertices[floatIndex++] = 0;
						SpriteVertices[floatIndex++] = 1;
						SpriteVertices[floatIndex++] = 0;
						floatIndex += 4;
						
						++numSprites;*/
						
						SetLabelVertices(label, screen.Cache.LastTexture, fontAtlas, textureRect, floatIndex);
						
						numSprites += label.Text.Length;
					}
				}
			}
			
			//SpriteVBO.SetVertices(SpriteVertices);
			SpriteVBO.SetVertices(SpriteVertices, 0, 0, numSprites * 4);
			
			Context.SetTexture(0, screen.Cache.LastTexture);
			Context.SetVertexBuffer(0, SpriteVBO);
			Context.DrawArrays(DrawMode.Triangles, 0, numSprites * 6);
		}
		
		private void SetLabelVertices(Label label, Texture2D texture, FontAtlas atlas, Rectangle textureRect, int floatIndex)
		{
			float originX = label.ClipFrame.X;
			float originY = label.ClipFrame.Y;
			Vector4 uvSpace = new Vector4(0, 0, 1, 1);
			
			for (int i = 0; i < label.Text.Length; ++i)
			{
				FontCharacter fntCharacter = atlas.CharacterMap[label.Text[i]];
				
				if (label.Wraps && (originX - label.ClipFrame.X + fntCharacter.XAdvance > label.ClipFrame.Width) || label.Text[i] == '\n')
				{
					originX = label.ClipFrame.X;
					originY += atlas.LineHeight;
					if (label.Text[i] == '\n'){
						++i;
					}
				}
				
				GetUVForChar(label.Text[i], atlas, textureRect.X, textureRect.Y, texture.Width, texture.Height, ref uvSpace);
				
				SpriteVertices[floatIndex++] = originX + fntCharacter.XOffset;
				SpriteVertices[floatIndex++] = originY + fntCharacter.YOffset;
				SpriteVertices[floatIndex++] = uvSpace.X;
				SpriteVertices[floatIndex++] = uvSpace.Y;
				// Skip past the color
				floatIndex += 4;
				
				SpriteVertices[floatIndex++] = originX + fntCharacter.XOffset;
				SpriteVertices[floatIndex++] = originY + fntCharacter.YOffset + fntCharacter.Height;
				SpriteVertices[floatIndex++] = uvSpace.X;
				SpriteVertices[floatIndex++] = uvSpace.W;
				floatIndex += 4;
				
				SpriteVertices[floatIndex++] = originX + fntCharacter.XOffset + fntCharacter.Width;
				SpriteVertices[floatIndex++] = originY + fntCharacter.YOffset + fntCharacter.Height;
				SpriteVertices[floatIndex++] = uvSpace.Z;
				SpriteVertices[floatIndex++] = uvSpace.W;
				floatIndex += 4;
				
				SpriteVertices[floatIndex++] = originX + fntCharacter.XOffset + fntCharacter.Width;
				SpriteVertices[floatIndex++] = originY + fntCharacter.YOffset;
				SpriteVertices[floatIndex++] = uvSpace.Z;
				SpriteVertices[floatIndex++] = uvSpace.Y;
				floatIndex += 4;
				
				originX += fntCharacter.XAdvance;
				
			}
		}
		
		private int GetTextWidth(string text, FontAtlas atlas)
		{
			int textWidth = 0;
			for (int letterLoop = 0; letterLoop < text.Length; ++letterLoop)
			{
				FontCharacter character;
				atlas.CharacterMap.TryGetValue(text[letterLoop], out character);
				textWidth += character.XAdvance;
			}
			
			return textWidth;
		}
		
		private int GetCharWidth(int character, FontAtlas atlas)
		{
			FontCharacter fntCharacter;
			atlas.CharacterMap.TryGetValue(character, out fntCharacter);
			return fntCharacter.XAdvance;	
		}
		
		// offsetX, offsetY in texture since in a packed texture
		private void GetUVForChar(int charID, FontAtlas atlas, float offsetTextureX, float offsetTextureY, float textureWidth, float textureHeight, ref Vector4 uvSpace)
		{
			FontCharacter fontChar;
			
			if (atlas.CharacterMap.TryGetValue(charID, out fontChar))
			{
				uvSpace.X = (offsetTextureX + fontChar.X)/textureWidth;
				uvSpace.Y = (offsetTextureY + fontChar.Y)/textureHeight; 
				uvSpace.Z = (offsetTextureX + fontChar.X + fontChar.Width)/textureWidth;
				uvSpace.W = (offsetTextureY + fontChar.Y + fontChar.Height)/textureHeight;
			}
			else
			{
				uvSpace.X = 0;
				uvSpace.Y = 0;
				uvSpace.Z = 1;
				uvSpace.W = 1;
			}
		}
	}
}

