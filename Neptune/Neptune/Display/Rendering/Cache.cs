using System;
using System.Collections.Generic;
using Sce.PlayStation.Core;
using Sce.PlayStation.Core.Imaging;
using Sce.PlayStation.Core.Graphics;

using Neptune.Display.Atlas;
using Neptune.Utility;

namespace Neptune.Display.Rendering
{
	public class Cache
	{
		public static int MAX_TEXTURES = 6;
		
		public int Size; 
		public TextureCube TextureCube;
		public Dictionary<string, Rectangle> Atlas; // Contains TextureKeys and their frame data.
		public List<TextureAtlas> Textures;
		public Texture2D LastTexture;
		public Dictionary<string, FontAtlas> Fonts;
		public Dictionary<string, Mesh> StaticMeshes;
		
		public Cache()
		{
			Atlas = new Dictionary<string, Rectangle>();
			Textures = new List<TextureAtlas>();
			Fonts = new Dictionary<string, FontAtlas>();
			StaticMeshes = new Dictionary<string, Mesh>();
		}
		
		public void CompressTextures()
		{
			if (Textures.Count > 0)
			{
				Image image = new Image(Textures[0].TextureFile);
				image.Decode();
				Size = image.Size.Width;
				
				TextureCube = new TextureCube(Size, false, PixelFormat.Rgba);
				
				for (int i = 0; i < Textures.Count; ++i)
				{
					image = new Image(Textures[i].TextureFile);
					image.Decode();
					TextureCube.SetPixels(0, (TextureCubeFace)i, image.ToBuffer());
				}
			}
		}
		
		public void ClearTextures()
		{
			Textures.Clear();
			Atlas.Clear();
		}
		
		public Rectangle GetRectangleForTextureKey(string key)
		{
			Rectangle rect;
			
			if (Atlas.TryGetValue(key, out rect)) {
				return rect;
			}

			return new Rectangle(0, 0, Size, Size);
		}
		
		public void AddTexture(string texturefile, Dictionary<string, Rectangle> atlas)
		{
			if (Textures.Count < MAX_TEXTURES)
			{
				Textures.Add(new TextureAtlas(texturefile, atlas));
				
				foreach(KeyValuePair<string, Rectangle> entry in atlas)
				{
					if (Atlas.ContainsKey(entry.Key))
					{
						throw new Exception("The key \"" + entry.Key + "\" already exists in the global atlas. Please use another name.");
					}
					Atlas[entry.Key] = entry.Value;
				}
				
				LastTexture = new Texture2D(texturefile, true);
			}
			else {
				throw new Exception("You have exceeded the number of faces. Please remove or empty before adding more.");
			}
		}
		
		public void RemoveTexture(string texturefile)
		{
			foreach (TextureAtlas atlas in Textures)
			{
				if (atlas.TextureFile == texturefile)
				{
					Textures.Remove(atlas);
					break;
				}
			}
		}
		
		public void AddFont(string fontkey, FontAtlas atlas)
		{
			if (!Fonts.ContainsKey(fontkey))
			{
				Fonts.Add(fontkey, atlas);
			}
			else {
				throw new Exception("Font already exists.");
			}
		}
		
		public void RemoveFont(string fontkey)
		{
			// TODO ...	
		}
		
		public void AddMesh(string meshkey, Mesh mesh)
		{
			StaticMeshes.Add(meshkey, mesh);
		}
		
		public void RemoveMesh(string meshkey)
		{
			StaticMeshes.Remove(meshkey);
		}
	}
}