using System;
using System.Collections.Generic;

using Sce.PlayStation.Core;
using Sce.PlayStation.Core.Graphics;
using Sce.PlayStation.Core.Imaging;

namespace Neptune.Display.Rendering
{
	public class Mesh
	{
		public struct MeshDesc
		{
			// Vertex index offset and count
			public int Offset;
			public int Count;
			public Vector4 Color;
			
			public MeshDesc(int offset, int count, Vector4 color)
			{
				Offset = offset;
				Count = count;
				Color = color;
			}
		}
		
		public VertexBuffer VertexBuffer;
		
		public List<MeshDesc> MeshDescriptions;
		
		public Mesh(VertexBuffer vbo, List<MeshDesc> meshDescriptions)
		{
			VertexBuffer = vbo;
			MeshDescriptions = meshDescriptions;
		}
	}
}