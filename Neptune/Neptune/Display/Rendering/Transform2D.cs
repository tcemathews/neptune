using System;
using Sce.PlayStation.Core;
using Neptune.Display;

namespace Neptune.Display.Rendering
{
	public class Transform2D
	{
		public float Rotation; // The angle at which the content is rotated from the origin. Degrees? Radians?
		public Vector2 Origin; // The point at which transformation occurs. 0, 0 is top left. 1, 1 is bottom right.
		public Vector2 Scale; // The size at which the graphic is rendered. 1, 1 is actual size.
		public Vector2 Shear; // No idea how to use this.
		public Color Color; // The color transformation to be applied to the graphic when rendered.
		
		public Transform2D ()
		{
			Rotation = 0.0f;
			Origin.X = 0.0f;
			Origin.Y = 0.0f;
			Scale.X = 1.0f;
			Scale.Y = 1.0f;
			Shear.X = 0.0f;
			Shear.Y = 0.0f;
			Color = new Color(255, 255, 255, 255);
		}
	}
}

