using System.Collections.Generic;

using Sce.PlayStation.Core;
using Sce.PlayStation.Core.Graphics;
using Sce.PlayStation.Core.Imaging;

namespace Neptune.Display.Rendering
{
	public class Light
	{
		public Vector3 SpecularColor;
		public Vector3 DiffuseColor;
		public Vector3 Position;
		
		public Light() 
		{
			SpecularColor = new Vector3(1.0f);
			DiffuseColor = new Vector3(1.0f);
			Position = new Vector3(0.0f);
		}
	}
	
	public class Lighting
	{
		public Vector3 AmbientColor;
		public List<Light> Lights;
		
		public Lighting ()
		{
			AmbientColor = new Vector3(.25f, .25f, .25f);
		}
	}
}
