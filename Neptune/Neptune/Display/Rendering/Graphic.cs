using System;
using System.Collections.Generic;
using Sce.PlayStation.Core;
using Neptune.Display.Rendering;

namespace Neptune.Display.Rendering
{
	public class Graphic : IComparable<Graphic>
	{
		public bool InStack; // Used by contoller to tell others that it is in the stack.
		public bool Visible; // Determines if the graphic will be rendered by the system on the render call.
		public float X; // Position horizontally on the screen to render at from the origin.
		public float Y; // Position vertically on the screen to render at from the origin.
		public float Z; // Position in depth on the screen to render at.
		public Transform2D Transform2D; // Transformations to apply for 2D graphics.
		public Transform3D Transform3D; // Transformations to apply for 3D graphics.
		public int Dimensions;  // Either 2 or 3 - query for Screen.Add
		
		public Graphic ()
		{
			Dimensions = 2;
			InStack = false;
			Visible = true;
			X = 0.0f;
			Y = 0.0f;
			Z = 0.0f;
			Transform2D = new Transform2D();
			Transform3D = new Transform3D();
		}
		
		public int CompareTo(Graphic other)
		{
			return Z.CompareTo(other.Z);
		}
	}
}

